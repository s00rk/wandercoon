# -*- coding: utf-8 -*-
# Generated by Django 1.9.2 on 2016-04-17 19:25
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('wandercoon_app', '0013_lugar_wanderden'),
    ]

    operations = [
        migrations.AlterField(
            model_name='lugar',
            name='expiracion',
            field=models.DateTimeField(blank=True, null=True),
        ),
    ]

# -*- coding: utf-8 -*-
# Generated by Django 1.9.2 on 2016-04-09 22:57
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('wandercoon_app', '0007_lugar_consumo_promedio'),
    ]

    operations = [
        migrations.AddField(
            model_name='lugar',
            name='categoria',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='wandercoon_app.Categoria'),
        ),
    ]

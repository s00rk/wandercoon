# -*- coding: utf-8 -*-
# Generated by Django 1.9.6 on 2016-08-13 17:58
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('wandercoon_app', '0030_remove_lugar_color_pin'),
    ]

    operations = [
        migrations.AddField(
            model_name='categoria',
            name='color',
            field=models.CharField(default='#fff', max_length=7),
        ),
    ]

# -*- coding: utf-8 -*-
# Generated by Django 1.9.6 on 2016-08-04 18:36
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('wandercoon_app', '0027_auto_20160803_1215'),
    ]

    operations = [
        migrations.AlterField(
            model_name='lugar',
            name='descripcion',
            field=models.TextField(blank=True, max_length=250),
        ),
        migrations.AlterField(
            model_name='lugar',
            name='descripcion_ingles',
            field=models.TextField(blank=True, max_length=250),
        ),
        migrations.AlterField(
            model_name='ruta',
            name='descripcion',
            field=models.TextField(blank=True, default='', max_length=500),
        ),
        migrations.AlterField(
            model_name='usuarioruta',
            name='descripcion',
            field=models.TextField(blank=True, default='', max_length=500, null=True),
        ),
    ]

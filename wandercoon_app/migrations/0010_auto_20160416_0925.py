# -*- coding: utf-8 -*-
# Generated by Django 1.9.2 on 2016-04-16 15:25
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('wandercoon_app', '0009_auto_20160410_1240'),
    ]

    operations = [
        migrations.AlterField(
            model_name='lugar',
            name='verificado',
            field=models.CharField(choices=[('no activo', 'No Activo'), ('pendiente', 'Pendiente'), ('verificado', 'Verificado'), ('no verificado', 'No Verificado')], default='No Activo', max_length=30),
        ),
    ]

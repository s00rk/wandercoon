# -*- coding: utf-8 -*-
# Generated by Django 1.9.6 on 2016-06-25 17:41
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('wandercoon_app', '0025_ruta_transporte'),
    ]

    operations = [
        migrations.AddField(
            model_name='ruta',
            name='indicacion',
            field=models.TextField(blank=True, default=''),
        ),
    ]

# -*- coding: utf-8 -*-
# Generated by Django 1.9.6 on 2016-08-13 17:55
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('wandercoon_app', '0028_auto_20160804_1236'),
    ]

    operations = [
        migrations.AddField(
            model_name='lugar',
            name='color_pin',
            field=models.CharField(default='#fff', max_length=7),
        ),
    ]

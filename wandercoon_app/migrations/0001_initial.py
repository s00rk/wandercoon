# -*- coding: utf-8 -*-
# Generated by Django 1.9.2 on 2016-03-01 05:29
from __future__ import unicode_literals

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Categoria',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('nombre', models.CharField(max_length=100, unique=True)),
            ],
        ),
        migrations.CreateModel(
            name='Empresa',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('nombre', models.CharField(max_length=100)),
                ('logo', models.ImageField(null=True, upload_to='company')),
                ('categoria', models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, to='wandercoon_app.Categoria')),
                ('usuario', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.CreateModel(
            name='Logro',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('nombre', models.CharField(max_length=100)),
            ],
        ),
        migrations.CreateModel(
            name='Lugar',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('nombre', models.CharField(max_length=100)),
                ('imagen', models.ImageField(blank=True, null=True, upload_to='lugar')),
                ('lat', models.FloatField()),
                ('lon', models.FloatField()),
                ('descripcion', models.TextField(blank=True)),
                ('descripcion_ingles', models.TextField(blank=True)),
                ('slug', models.SlugField(blank=True, editable=False, max_length=300)),
                ('verificado', models.BooleanField(default=False)),
                ('fecha_verificado', models.DateField(blank=True, null=True)),
                ('traducir', models.BooleanField(default=False)),
                ('hora_entrada', models.CharField(blank=True, max_length=100)),
                ('hora_salir', models.CharField(blank=True, max_length=100)),
                ('direccion', models.TextField(blank=True)),
                ('telefonos', models.CharField(blank=True, max_length=100)),
                ('expiracion', models.DateTimeField(auto_now_add=True, null=True)),
                ('empresa', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='wandercoon_app.Empresa')),
            ],
            options={
                'verbose_name_plural': 'Lugares',
            },
        ),
        migrations.CreateModel(
            name='Ruta',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('nombre', models.CharField(max_length=100)),
                ('categorias', models.ManyToManyField(to='wandercoon_app.Categoria')),
                ('lugares', models.ManyToManyField(to='wandercoon_app.Lugar')),
            ],
        ),
        migrations.CreateModel(
            name='Tag',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('nombre', models.CharField(max_length=100, unique=True)),
            ],
        ),
        migrations.CreateModel(
            name='UsuarioCategoria',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('estrellas', models.IntegerField()),
                ('categoria', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='wandercoon_app.Categoria')),
                ('usuario', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.CreateModel(
            name='UsuarioLogro',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('logros', models.ManyToManyField(to='wandercoon_app.Logro')),
                ('usuario', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.CreateModel(
            name='UsuarioRuta',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('nombre', models.CharField(max_length=100)),
                ('compartir', models.BooleanField(default=True)),
                ('lugares', models.ManyToManyField(blank=True, to='wandercoon_app.Lugar')),
                ('ruta', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='wandercoon_app.Ruta')),
                ('usuario', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.AddField(
            model_name='ruta',
            name='tags',
            field=models.ManyToManyField(to='wandercoon_app.Tag'),
        ),
        migrations.AddField(
            model_name='lugar',
            name='tags',
            field=models.ManyToManyField(blank=True, to='wandercoon_app.Tag'),
        ),
    ]

# -*- coding: utf-8 -*-
# Generated by Django 1.9 on 2016-04-18 00:25
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('wandercoon_app', '0014_auto_20160417_1325'),
    ]

    operations = [
        migrations.RenameField(
            model_name='lugar',
            old_name='fecha_verificado',
            new_name='fecha_verificacion',
        ),
        migrations.AddField(
            model_name='lugar',
            name='verificacion_expiracion',
            field=models.DateField(blank=True, null=True),
        ),
    ]

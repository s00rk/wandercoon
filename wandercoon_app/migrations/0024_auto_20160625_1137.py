# -*- coding: utf-8 -*-
# Generated by Django 1.9.6 on 2016-06-25 17:37
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('wandercoon_app', '0023_usuariolugar'),
    ]

    operations = [
        migrations.AlterField(
            model_name='ruta',
            name='descripcion',
            field=models.TextField(blank=True, default='', max_length=144),
        ),
    ]

from django.conf.urls import url, include
from django.views.decorators.csrf import csrf_exempt

from .api import OlvidoView, LanguageListView, CountryListView, CategoriaView, UsuarioCategoriaView, UsuarioAddTag, LoginView, RegistroView, RecuperarView, LugarView, LugarSingleView, UsuarioLugarView, RutaUsuario, RutaUsuarioAdd, RutaUsuarioDeleteView, RutaUsuarioRemove, RutaUsuarioSingleView, RutaUsuarioEditView, RutaUsuarioImportView, RutaSingleView, RutasView, ProfileView, TagView, ChangePasswordView, ProfileEditView, RutaCreateUsuario, SuggestedRoutesView, PhotoProfileUploadView, RouteListNewView, InterestSelectView, InterestVotes, register_by_access_token, AuthFacebookView,UsuarioRutaUploadImagen,RutaUsuarioVoteView

urls = [
	url(r'^login/$', csrf_exempt(LoginView.as_view())),
	url(r'^login/facebook', csrf_exempt(AuthFacebookView.as_view())),
	url(r'^registro/', csrf_exempt(RegistroView.as_view())),
	url(r'^restaurar/', csrf_exempt(RecuperarView.as_view())),
	url(r'^categoria/', CategoriaView.as_view()),
	url(r'^lugar/$', LugarView.as_view()),
	url(r'^lugar/(?P<lugar_id>\d+)/$', LugarSingleView.as_view()),
	url(r'^usuario-lugar/$', csrf_exempt(UsuarioLugarView.as_view())),
	url(r'^usuario-categoria/$', csrf_exempt(UsuarioCategoriaView.as_view())),
	url(r'^usuario-ruta/$', csrf_exempt(RutaUsuarioVoteView.as_view())),
	url(r'^rutas/usuario/$', csrf_exempt(RutaUsuario.as_view())),
	url(r'^rutas/usuario/(?P<route_id>\d+)/$', RutaUsuarioSingleView.as_view()),
	url(r'^rutas/usuario/(?P<route_id>\d+)/photo-upload/$', csrf_exempt(UsuarioRutaUploadImagen.as_view())),
	url(r'^rutas/usuario/editar/(?P<route_id>\d+)/$', RutaUsuarioEditView.as_view()),
	url(r'^rutas/usuario/crear/$', csrf_exempt(RutaCreateUsuario.as_view())),
	url(r'^rutas/usuario/agregar/$', csrf_exempt(RutaUsuarioAdd.as_view())),
	url(r'^rutas/usuario/remove/$', csrf_exempt(RutaUsuarioRemove.as_view())),
	url(r'^rutas/usuario/eliminar/(?P<route_id>\d+)/$', csrf_exempt(RutaUsuarioDeleteView.as_view())),
	url(r'^rutas/usuario/importar/$', csrf_exempt(RutaUsuarioImportView.as_view())),
	url(r'^rutas/$', csrf_exempt(RutasView.as_view())),
	url(r'^rutas/(?P<route_id>\d+)/$', RutaSingleView.as_view()),
	url(r'^perfil/$', csrf_exempt(ProfileView.as_view())),
	url(r'^perfil/recuperar/$', csrf_exempt(OlvidoView.as_view())),
	url(r'^perfil/actualizar/$', csrf_exempt(ProfileEditView.as_view())),
	url(r'^perfil/agregar-tag/$', csrf_exempt(UsuarioAddTag.as_view())),
	url(r'^perfil/photo-upload/$', csrf_exempt(PhotoProfileUploadView.as_view())),
	url(r'^cambiar-contrasena/$', csrf_exempt(ChangePasswordView.as_view())),
	url(r'^tags/', TagView.as_view()),
	url(r'^country-list/', CountryListView.as_view()),
	url(r'^language-list/', LanguageListView.as_view()),
	url(r'^sugeridos-rutas/', RouteListNewView.as_view()),
	url(r'^intereses-list/', InterestSelectView.as_view()),
	url(r'^intereses-votes/', InterestVotes.as_view()),
	url(r'^register-by-token/(?P<backend>[^/]+)', register_by_access_token),
    # Python Social Auth URLs
	url('', include('social.apps.django_app.urls', namespace='social')),
]

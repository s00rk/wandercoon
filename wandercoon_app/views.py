# -*- encoding: utf-8 -*-
from django.conf import settings
from django.shortcuts import render, redirect
from django.contrib.auth import authenticate, login
from django.contrib import messages
from django.views.generic.edit import DeleteView
from django.views.generic import TemplateView
from django.utils import timezone

from django.core.urlresolvers import reverse_lazy

from .models import Lugar, Empresa, Tag, Categoria
from pago.models import Compra, Combo, Carrito
from decimal import Decimal

import datetime

class PanelView(TemplateView):
	template_name = 'inicio.html'
	def get(self, request):
		return redirect('mis-locales')

class HomeView(TemplateView):
	template_name = 'web/index.html'

	def post(self, request):
		registro = request.POST.get('registro')
		if registro is not None:
			usuario = request.POST.get('usuario')
			contrasena = request.POST.get('contrasena')
			re_contrasena = request.POST.get('re_contrasena')
			email = request.POST.get('email')
			telefono = request.POST.get('telefono')

		return render(request, self.template_name)

class LocalView(TemplateView):
	template_name = 'locales/mis-locales.html'

	def get(self, request):
		today = timezone.now()
		Carrito.objects.filter(fecha__lt=(today - datetime.timedelta(days=2))).delete()
		cart = Carrito.objects.filter(usuario=request.user)
		subtotal = Decimal(0)
		for p in cart:
			subtotal += p.total
		igv = subtotal * Decimal(settings.IGV)
		total = subtotal + igv

		combos_local = Combo.objects.order_by('id')[2:4]
		locales = Lugar.objects.filter(empresa__usuario=request.user).order_by('-id')
		return render(request, self.template_name, {'locales': locales, 'combos_local': combos_local, 'igv': igv, 'total': total, 'cart': cart, 'subtotal': subtotal})

class LocalDeleteView(DeleteView):
	model = Lugar
	success_url = reverse_lazy('mis-locales')

class LocalNewView(TemplateView):
	template_name = 'locales/mi-local-new.html'

	def get(self, request):
		total = Lugar.objects.filter(empresa__usuario=request.user).count()
		if total >= 6:
			messages.success(request, 'Has excedido la cantidad de locales gratis, si deseas mas deberas comprarlo')
			return redirect('/panel/')


		tags = Tag.objects.all().order_by('nombre')
		categorias = Categoria.objects.exclude(nombre__icontains='wander')
		return render(request, self.template_name, {'tags': tags, 'categorias': categorias})

	def post(self, request):
		total = Lugar.objects.filter(empresa__usuario=request.user).count()
		if total >= 6:
			messages.success(request, 'Has excedido la cantidad de locales gratis, si deseas mas deberas comprarlo')
			return redirect('/panel/')

		imagen = request.FILES.get('imagen')
		nombre = request.POST.get('nombre')
		descripcion = request.POST.get('descripcion')
		lati = request.POST.get('lati')
		longi = request.POST.get('longi')
		telefonos = request.POST.get('telefonos')
		entrada = request.POST.get('entrada')
		salida = request.POST.get('salida')
		direccion = request.POST.get('direccion')
		descripcion_ingles = request.POST.get('descripcion_ingles')
		tags = request.POST.getlist('tags')
		consumo_promedio = request.POST.get('consumo_promedio')
		banner = request.POST.get('banner', '')
		banner_ingles = request.POST.get('banner_ingles', '')
		categoria = request.POST.get('categoria')

		if nombre is None or descripcion is None or lati is None or longi is None or telefonos is None:
			pass

		lugar = Lugar()
		if imagen:
			lugar.imagen = imagen
		lugar.empresa = Empresa.objects.filter(usuario=request.user).first()
		lugar.nombre = nombre
		lugar.descripcion = descripcion
		lugar.descripcion_ingles = descripcion_ingles
		lugar.lat = lati
		lugar.lon = longi
		lugar.hora_entrada = entrada
		lugar.hora_salir = salida
		lugar.direccion = direccion
		lugar.telefonos = telefonos
		lugar.consumo_promedio = consumo_promedio

		lugar.save()
		lugar.tags.clear()
		if tags is not None:
			for tag in tags:
				t = Tag.objects.get(id=tag)
				lugar.tags.add(t)
		if categoria is not None and len(categoria) > 0:
			categoria = Categoria.objects.get(pk=categoria)
		lugar.banner = banner
		lugar.banner_ingles = banner_ingles
		lugar.categoria = categoria
		lugar.save()
		messages.success(request, 'Local Creado Satisfactoriamente')
		return redirect('mis-locales')

class LocalEditView(TemplateView):
	template_name = 'locales/mi-local-edit.html'
	def get(self, request, slug):
		local = Lugar.objects.get(slug=slug)
		if local is None:
			return redirect('panel')
		tags = Tag.objects.all().order_by('nombre')
		categorias = Categoria.objects.exclude(nombre__icontains='wander')
		return render(request, self.template_name, {'local': local, 'tags': tags, 'categorias':categorias})

	def post(self, request, slug):
		imagen = request.FILES.get('imagen')
		nombre = request.POST.get('nombre')
		descripcion = request.POST.get('descripcion')
		lati = request.POST.get('lati')
		longi = request.POST.get('longi')
		telefonos = request.POST.get('telefonos')
		entrada = request.POST.get('entrada')
		salida = request.POST.get('salida')
		direccion = request.POST.get('direccion')
		tags = request.POST.getlist('tags')
		descripcion_ingles = request.POST.get('descripcion_ingles')
		consumo_promedio = request.POST.get('consumo_promedio')
		banner = request.POST.get('banner')
		banner_ingles = request.POST.get('banner_ingles')
		categoria = request.POST.get('categoria')

		if nombre is None or descripcion is None or lati is None or longi is None or telefonos is None:
			pass

		lugar = Lugar.objects.get(slug=slug)

		lugar.tags.clear()
		if tags is not None:
			for tag in tags:
				t = Tag.objects.get(id=tag)
				lugar.tags.add(t)
		if categoria is not None and len(categoria) > 0:
			categoria = Categoria.objects.get(pk=categoria)

		if imagen:
			lugar.imagen = imagen
		lugar.empresa = Empresa.objects.filter(usuario=request.user).first()
		lugar.nombre = nombre
		lugar.descripcion = descripcion
		lugar.descripcion_ingles = descripcion_ingles
		lugar.lat = lati
		lugar.lon = longi
		lugar.hora_entrada = entrada
		lugar.hora_salir = salida
		lugar.direccion = direccion
		lugar.telefonos = telefonos
		lugar.consumo_promedio = consumo_promedio
		lugar.banner = banner
		lugar.banner_ingles = banner_ingles
		lugar.categoria = categoria
		lugar.save()
		messages.success(request, 'Local Modificado Satisfactoriamente')
		return redirect('mis-locales')

class ComprasView(TemplateView):
	template_name = 'compras/mis-compras.html'
	template_name2 = 'compras/compra_detalle.html'

	def get(self, request):
		compra = Compra.objects.filter(pk=request.GET.get('codigo')).first()
		if compra is not None:
			return render(request, self.template_name2, {'compra':compra})

		compras = Compra.objects.filter(usuario=request.user, estado='venta_exitosa').order_by('-id')
		return render(request, self.template_name, { 'compras': compras })

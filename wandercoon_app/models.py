from __future__ import unicode_literals

from django.conf import settings
from django.db import models
from django.contrib.auth.models import User
from django.template.defaultfilters import slugify
from django.dispatch.dispatcher import receiver
from django.db.models.signals import post_save, post_delete
from django.core.urlresolvers import reverse
from django.utils import timezone
import os, datetime, random

class Categoria(models.Model):
	nombre        = models.CharField(max_length=100, unique=True)
	nombre_ingles = models.CharField(max_length=100, default="", blank=True)
	color         = models.CharField(max_length=7, default="#fff")
	estrellas     = models.IntegerField(default=0, blank=True)

	def __unicode__(self):
		return u'%s' % self.nombre

class UsuarioCategoria(models.Model):
	usuario = models.ForeignKey(User)
	categoria = models.ForeignKey(Categoria)
	estrellas = models.IntegerField()

	def __unicode__(self):
		return u'%s' % self.usuario.username

class Empresa(models.Model):
	usuario = models.ForeignKey(User)
	nombre = models.CharField(max_length=100)
	logo = models.ImageField(upload_to='company', null=True)
	categoria = models.ForeignKey(Categoria, null=True)

	def __unicode__(self):
		return u'%s' % self.nombre

class Tag(models.Model):
	nombre = models.CharField(max_length=100, unique=True)

	def __unicode__(self):
		return u'%s' % self.nombre

class Lugar(models.Model):
	activo = models.BooleanField(default=False)
	wanderden = models.BooleanField(default=False)
	empresa = models.ForeignKey(Empresa)
	nombre = models.CharField(max_length=100)
	imagen = models.ImageField(upload_to='lugar', null=True, blank=True)
	lat = models.FloatField(null=True, blank=True)
	lon = models.FloatField(null=True, blank=True)
	descripcion = models.TextField(blank=True, max_length=250)
	descripcion_ingles = models.TextField(blank=True, max_length=250)
	tags = models.ManyToManyField(Tag, blank=True)
	slug = models.SlugField(max_length=300, blank=True, editable=False)
	verfs = (
		('no activo', 'No Activo'),
		('pendiente', 'Pendiente'),
		('verificado', 'Verificado'),
		('no verificado', 'No Verificado')
	)
	verificado = models.CharField(max_length=30, choices=verfs, default='no activo')
	fecha_verificacion = models.DateField(auto_now=False, null=True, blank=True)
	verificacion_expiracion = models.DateField(auto_now=False, null=True, blank=True)
	traducir = models.BooleanField(default=False)
	hora_entrada = models.CharField(max_length=100, blank=True)
	hora_salir = models.CharField(max_length=100, blank=True)
	direccion = models.TextField(blank=True)
	telefonos = models.CharField(max_length=100, blank=True)
	expiracion = models.DateTimeField(blank=True, null=True)
	categoria = models.ForeignKey(Categoria, null=True, blank=True)

	ruta_recomendada = models.BooleanField(default=False)
	ruta_recomendada_expiracion = models.DateTimeField(default=timezone.now, blank=True, null=True)

	banner            = models.CharField(max_length=70, blank=True)
	banner_ingles     = models.CharField(max_length=70, blank=True, default="")
	banner_activado   = models.BooleanField(default=False)
	banner_expiracion = models.DateTimeField(default=timezone.now,blank=True, null=True)

	consumo_promedio = models.CharField(max_length=10, blank=True)
	estrellas = models.IntegerField(default=0, blank=True)

	def expiro(self):
		if self.activo and self.expiracion is None:
			return False
		elif not self.activo and self.expiracion is None:
			return True

		hoy = timezone.now()
		exp = self.expiracion
		if hoy > exp:
			return True
		return False
	def expiro_banner(self):
		hoy = timezone.now()
		exp = self.banner_expiracion
		if hoy > exp:
			return True
		return False
	def expiro_ruta(self):
		hoy = timezone.now()
		exp = self.ruta_recomendada_expiracion
		if hoy > exp:
			return True
		return False

	def get_absolute_url(self):
		return reverse('mis-locales', kwargs={'slug': self.slug})

	def esta_verificado(self):
		status = False
		if self.verificado == "verificado":
			status = True

		return status

	class Meta:
		verbose_name_plural = 'Lugares'

	def __unicode__(self):
		return u'%s - %s' % (self.nombre, self.empresa.nombre)

	def save(self, *args, **kwargs):
		if self.pk is None:
			self.slug = slugify(self.nombre + str(random.randint(1, 10000)))
			while Lugar.objects.filter(slug=self.slug).exists():
				self.slug = slugify(self.nombre + str(random.randint(1, 10000)))
		else:
			self.slug = slugify(self.nombre+str(self.id))
		super(Lugar, self).save(*args, **kwargs)

class UsuarioLugar(models.Model):
	usuario   = models.ForeignKey(User)
	lugar     = models.ForeignKey(Lugar)
	estrellas = models.IntegerField()

	def __unicode__(self):
		return u'%s' % self.usuario.username

class Logro(models.Model):
	nombre = models.CharField(max_length=100)

	def __unicode__(self):
		return u'%s' % self.nombre

class UsuarioLogro(models.Model):
	usuario = models.ForeignKey(User)
	logros = models.ManyToManyField(Logro)

	def __unicode__(self):
		return u'%s' % self.usuario.username

class Ruta(models.Model):
	nombre             = models.CharField(max_length=100)
	nombre_ingles      = models.CharField(max_length=100, default="", blank=True)
	descripcion        = models.TextField(default="", blank=True, max_length=500)
	descripcion_ingles = models.TextField(default="", blank=True, max_length=500)
	imagen             = models.ImageField(upload_to='rutas', null=True, blank=True)
	indicacion         = models.TextField(default="", blank=True)
	indicacion_ingles  = models.TextField(default="", blank=True)
	lugares            = models.ManyToManyField(Lugar)
	categorias         = models.ManyToManyField(Categoria)
	tags               = models.ManyToManyField(Tag)
	TIPOS_TRANS = (
		('pie', 'A pie'),
		('bus', 'Bus'),
		('taxi', 'Taxi'),
		('bike', 'Bicicleta'),
	)
	transporte         = models.CharField(max_length=100, choices=TIPOS_TRANS, default="pie")
	cantidad_usuarios  = models.IntegerField(default=0)

	def __unicode__(self):
		return u'%s' % self.nombre

class UsuarioRuta(models.Model):
	nombre      = models.CharField(max_length=100)
	descripcion = models.TextField(max_length=500, default="", blank=True, null=True)
	usuario     = models.ForeignKey(User)
	lugares     = models.ManyToManyField(Lugar, blank=True)
	#ruta        = models.ForeignKey(Ruta, blank=True, null=True)
	imagen      = models.ImageField(upload_to='rutas_usuario', null=True, blank=True)
	compartir   = models.BooleanField(default=True)
	tags        = models.ManyToManyField(Tag, blank=True)
	indicacion  = models.TextField(default="", blank=True)
	TIPOS_TRANS = (
		('pie', 'A pie'),
		('bus', 'Bus'),
		('taxi', 'Taxi'),
		('bike', 'Bicicleta'),
	)
	transporte  = models.CharField(max_length=100, choices=TIPOS_TRANS, default="pie")
	categorias  = models.ManyToManyField(Categoria, default="", blank=True)

	def __unicode__(self):
		return self.nombre

class UsuarioRutaVote(models.Model):
	ruta      = models.ForeignKey(Ruta)
	usuario   = models.ForeignKey(User)
	estrellas = models.IntegerField()

	def __unicode__(self):
		return u'%s' % self.usuario.username

@receiver(post_save , sender=Lugar)
def rename_logos(sender, instance, **kwargs):
	created = False
	if 'created' in kwargs:
		if kwargs['created']:
			created = True
	if instance.imagen and not created:
		ext = instance.imagen.name.split('.')[-1]
		filename = 'lugar/{}.{}'.format(instance.pk, ext)
		direccion = instance.imagen.path
		dir_file = os.path.join(settings.MEDIA_ROOT, filename)
		if str(direccion) != str(dir_file):
			if os.path.exists(dir_file):
				os.remove(dir_file)
			os.rename(direccion, dir_file)
			Lugar.objects.filter(pk=instance.pk).update(imagen=filename)

@receiver(post_delete , sender=Lugar)
def lugar_delete(sender, instance, **kwargs):
	instance.imagen.delete(False)

@receiver(post_save , sender=Empresa)
def rename_empresa(sender, instance, **kwargs):
	created = False
	if 'created' in kwargs:
		if kwargs['created']:
			created = True
	if instance.logo and not created:
		ext = instance.logo.name.split('.')[-1]
		filename = 'company/{}.{}'.format(instance.pk, ext)
		direccion = os.path.join(settings.MEDIA_ROOT, instance.logo.name)
		dir_file = os.path.join(settings.MEDIA_ROOT, filename)
		if str(direccion) != str(dir_file):
			if os.path.exists(dir_file):
				os.remove(dir_file)
			os.rename(direccion, dir_file)
			Empresa.objects.filter(pk=instance.pk).update(logo=filename)

@receiver(post_delete , sender=Empresa)
def empresa_delete(sender, instance, **kwargs):
	instance.logo.delete(False)

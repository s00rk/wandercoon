from django.apps import AppConfig


class WandercoonAppConfig(AppConfig):
    name = 'wandercoon_app'

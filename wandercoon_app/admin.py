from django.contrib import admin

from .models import Categoria, UsuarioCategoria, Empresa, Lugar, UsuarioLugar, Logro, UsuarioLogro, Ruta, UsuarioRuta, Tag,UsuarioRutaVote

@admin.register(Categoria)
class CategoriaAdmin(admin.ModelAdmin):
	list_display = ('nombre',)

@admin.register(Tag)
class TagAdmin(admin.ModelAdmin):
	list_display = ('nombre',)

@admin.register(UsuarioCategoria)
class UserCategoryAdmin(admin.ModelAdmin):
	list_display = ('usuario', 'categoria', 'estrellas')
	raw_id_fields = ('usuario',)

@admin.register(Empresa)
class CompanyAdmin(admin.ModelAdmin):
	list_display = ('nombre', 'usuario', 'categoria', 'logo')
	raw_id_fields = ('usuario',)

@admin.register(Lugar)
class PlaceAdmin(admin.ModelAdmin):
	list_display = ('nombre', 'empresa', 'lat', 'lon')
	raw_id_fields = ('empresa',)
	filter_horizontal = ('tags',)
	list_filter  = ('activo', 'verificado', 'wanderden')
	ordering = ['nombre']
	search_fields = ['nombre', 'empresa__nombre', 'descripcion', 'telefonos']
	exclude = ('estrellas',)

@admin.register(UsuarioLugar)
class PlaceAdmin(admin.ModelAdmin):
	list_display  = ('usuario', 'lugar', 'estrellas')
	raw_id_fields = ('lugar',)

@admin.register(Logro)
class AwardAdmin(admin.ModelAdmin):
	list_display = ('nombre',)

@admin.register(UsuarioLogro)
class UserAwardAdmin(admin.ModelAdmin):
	list_display = ('usuario',)
	filter_horizontal = ('logros',)
	raw_id_fields = ('usuario',)

@admin.register(Ruta)
class RouteAdmin(admin.ModelAdmin):
	list_display = ('nombre',)
	filter_horizontal = ('lugares', 'categorias', 'tags')

@admin.register(UsuarioRuta)
class UserRouteAdmin(admin.ModelAdmin):
	list_display = ('nombre', 'usuario', 'compartir')
	filter_horizontal = ('lugares', 'tags')
	raw_id_fields = ('usuario',)

@admin.register(UsuarioRutaVote)
class UsuarioRutaVoteAdmin(admin.ModelAdmin):
	list_display = ('ruta', 'usuario', 'estrellas')
	raw_id_fields = ('ruta', 'usuario')
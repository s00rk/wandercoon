# -*- coding: utf-8 -*-
from .models import Categoria, Lugar, UsuarioLugar, Empresa, Ruta, UsuarioRuta, Tag, UsuarioCategoria
from base64 import b64decode
from django.conf import settings
from django.contrib import messages
from django.contrib.auth import authenticate
from django.contrib.auth.models import User
from django.core import serializers
from django.core.exceptions import ObjectDoesNotExist
from django.core.files.base import ContentFile
from django.core.mail import EmailMessage
from django.db.models import Q
from django.http import JsonResponse, QueryDict
from django.shortcuts import render, redirect
from django.views.generic import View
from usuario.models import UsuarioInformacion
import uuid, urllib, base64, datetime, json, os, smtplib
from social.apps.django_app.utils import psa

def CheckToken(request):
	token_ = request.GET.get('token')
	if token_ is None:
		try:
			token_ = request.META['HTTP_APITOKEN']
		except:
			pass
	if token_ is not None:
		user = UsuarioInformacion.objects.filter(token=token_)
		if user.exists():
			return user.first()
		else:
			return 0
	return None

def CalcularEstrellasLugar(id):
	lugar = Lugar.objects.get(id=id)
	votos_usuarios = UsuarioLugar.objects.filter(lugar=lugar)

	votos = 0
	for voto in votos_usuarios:
		votos = votos + voto.estrellas

	try:
		estrellas = (int(votos) / int(votos_usuarios.count()))
	except ZeroDivisionError, e:
		estrellas = 0

	return estrellas

def CalcularEstrellasRuta(id):
	ruta = Ruta.objects.get(id=id)
	votos_usuarios = UsuarioRutaVote.objects.filter(ruta=ruta)

	votos = 0
	for voto in votos_usuarios:
		votos = votos + voto.estrellas

	try:
		estrellas = (int(votos) / int(votos_usuarios.count()))
	except ZeroDivisionError, e:
		estrellas = 0

	return estrellas

def APIResponse(data, message, success):
	settings = {
		"success": success,
		"message": message
	}
	if data is not None:
		return JsonResponse({"data": data, "settings":settings})
	else:
		return JsonResponse({"data": [], "settings": settings})

def ErrorAPIResponse(code, detail):
	errors = [
		{
			"status": code,
			"detail": detail
		}
	]
	return JsonResponse({"errors":errors}, safe=False)

class CountryListView(View):
	def get(self, request):
		#data = open(os.path.join(settings.BASE_DIR, 'static') + '/country.json').read()
		#"/home/kildare/Proyectos/Wandercoon/wandercoon_appstatic/country.json"		

		with open(os.path.join(settings.BASE_DIR, 'static') + '/country.json') as data_file:    
			data = json.load(data_file)		

		return APIResponse(data, "Country List", 1)

class LanguageListView(View):
	def get(self, request):
		data = [{
			"language": "Spanish",
			"code": "ES"
		}, {
			"language": "English",
			"code": "EN"
		}]

		return APIResponse(data, "Language List", 1)

class RutasView(View):
	def get(self,request):
		data = list(Ruta.objects.all().values())
		idioma = request.GET.get('lang')
		for d in data:
			dd = Ruta.objects.get(pk=d['id'])
			x = list(dd.lugares.values())

			if idioma == "EN":
				d['nombre'] = d['nombre_ingles']
				d['descripcion'] = d['descripcion_ingles']
				d['indicacion'] = d['indicacion_ingles']
			
			if d['imagen'] == None:
				d['imagen'] = ""
			else:
				d['imagen'] = settings.DOMINIO + dd.imagen.url

			d['lugares'] = x
			d['categorias'] = list(dd.categorias.all().values())
			i = 0
			for l in x:
				ll = Lugar.objects.get(pk=l['id'])

				p = ll.empresa

				#image = urllib.urlopen(settings.DOMINIO + p.logo.url)
				#logo = "data:image/jpeg;base64,%s"%base64.encodestring(image.read()).replace("\n","")

				if idioma == "EN":
					d['lugares'][i]['descripcion'] = d['lugares'][i]['descripcion_ingles']
					d['lugares'][i]['banner']      = d['lugares'][i]['banner_ingles']

				d['lugares'][i]['imagen'] = ""
				d['lugares'][i]['logo']   = ""

				if p.logo:
					logo = settings.DOMINIO + p.logo.url
					d['lugares'][i]['logo'] = logo

				if ll.imagen:
					imagen = settings.DOMINIO + ll.imagen.url
					d['lugares'][i]['imagen'] = imagen

				i += 1

		return APIResponse(data, "Rutas Sugeridas", 1)

class RutaUsuario(View):
	def get(self, request):
		user = CheckToken(request)
		idioma = request.GET.get('lang')
		if user is None:
			return APIResponse("", "Token Missing", 0)
		elif user == 0:
			return APIResponse("", "Token incorrect", 0)

		else:
			rutas = UsuarioRuta.objects.filter(usuario=user.usuario)
			
			if not rutas:
				success = 0
				if request.idioma == "EN":
					return APIResponse("", "The user dont have routes!", success)	
				return APIResponse("", "No existen rutas para el usuario indicado", success)
			else:
				success = 1

			rutas = list(rutas.values())

			i = 0
			for ruta in rutas:
				r = UsuarioRuta.objects.get(pk=ruta['id'])
				# Rutas
				lugares = list(r.lugares.all().values())
				ruta['lugares'] = lugares

				if r.imagen == None or r.imagen == "":
					ruta_imagen = ""
				else:
					ruta_imagen = settings.DOMINIO + r.imagen.url

				# show image
				ruta['imagen'] = ruta_imagen

				for l in lugares:
					ll = Lugar.objects.get(pk=l['id'])
					if idioma == "EN":
						ruta['lugares'][i]['descripcion'] = ruta['lugares'][i]['descripcion_ingles']
					i += 1

				# tags
				tags = list(r.tags.all().values())
				ruta['tags'] = tags

			return APIResponse(rutas, "Rutas Usuarios", success)

class RutaCreateUsuario(View):
	def get(self, request):
		"""
		POST /rutas/usuario -
		nombre: string, lugares: array - id, ruta: id, compartir: boolean
		descripcion: string, tags: array -id

		Example: JSON request
		{
			"nombre": "Esto es un nombre",
			"descripcion": "Hey hey hey aquí una descripción" or null,
			"tags":[279,280] or null,
			"lugares": [207, 206, 205] or null,
			"compartir": true or false,
			"ruta": 201 or null
		}
		"""
		user = CheckToken(request)
		if user is None:
			return APIResponse("", "Token Missing", 0)
		elif user == 0:
			return APIResponse("", "Token incorrect", 0)

		#JSON Data from mobile app
		#post_request = json.loads(request.body)

		nombre       = request.GET.get('nombre')
		descripcion  = request.GET.get('descripcion')
		indicaciones = request.GET.get('indicacion')
		usuario   	 = user
		transporte   = request.GET.get('transporte')
		#lugares   	 = request.GET.getlist('lugares')		
		ruta      	 = request.GET.get('ruta')
		# Param remove from view
		compartir 	 = False

		if nombre is None or transporte is None:
			return APIResponse("", "no puede estar vacio", 0)

		if compartir is None:
			compartir = False

		if transporte != "pie" and transporte != "bus" and transporte != "taxi":
			return APIResponse("", "Transporte incorrecto", 0)			

		nueva_ruta             = UsuarioRuta()
		nueva_ruta.nombre      = nombre
		nueva_ruta.descripcion = descripcion
		nueva_ruta.indicacion  = indicaciones
		nueva_ruta.transporte  = transporte
		nueva_ruta.usuario     = usuario.usuario
		nueva_ruta.compartir   = compartir

		if ruta is not None and ruta != "":
			try:
				r = Ruta.objects.get(id=int(ruta))
				nueva_ruta.ruta = r
			except ObjectDoesNotExist:
				return APIResponse("", "La ruta no existe", 0)

		nueva_ruta.save()
		"""
		if lugares is not None and lugares != "":
			for lugar in lugares:
				try:
					l = Lugar.objects.get(id=lugar)
					nueva_ruta.lugares.add(l)
				except ObjectDoesNotExist:
					return APIResponse("", "El lugar no existe", 0)

		lugares = []
		for lugar in nueva_ruta.lugares.all():
			lugares.append(lugar.id)
		"""
		data = [{	
			"usuario_id": usuario.usuario.id,
			"id": nueva_ruta.id,
			"nombre": nombre,
			"descripcion": descripcion,
			"transporte": transporte,
			"indicacion": indicaciones,
			#"ruta": nueva_ruta.ruta,
			#"lugares": lugares,
			"type": "ruta-usuario"
		}]
		return APIResponse(data, "Ruta creada", 1)


class RutaUsuarioImportView(View):
	def get(self, request):
		user = CheckToken(request)		

		if user is None:
			return APIResponse("", "Token Missing", 0)
		elif user == 0:
			return APIResponse("", "Token incorrect", 0)

		route_id = request.GET.get('route_id')

		if route_id is None:
			return APIResponse("", "Route id missing", 0)

		rutas = Ruta.objects.filter(id=route_id)
		if rutas.exists():
			ruta = Ruta.objects.get(id=route_id)
		else:
			return APIResponse("", "La ruta no existe", 0)		

		new_user_route                 = UsuarioRuta()

		if request.idioma == "EN":
			new_user_route.nombre      = ruta.nombre_ingles
			new_user_route.descripcion = ruta.descripcion_ingles
			new_user_route.indicacion  = ruta.indicacion_ingles
		else:
			new_user_route.nombre      = ruta.nombre
			new_user_route.descripcion = ruta.descripcion
			new_user_route.indicacion  = ruta.indicacion

		new_user_route.compartir       = False
		new_user_route.transporte      = ruta.transporte
		new_user_route.usuario         = user.usuario
		new_user_route.save()


		"""
		Add places to the new UserRoute
		"""
		places = list(ruta.lugares.all().values())
		for p in places:
			place = Lugar.objects.get(id=p['id'])
			new_user_route.lugares.add(place)

		"""
		Add tags to the new UserRoute
		"""
		tags = list(ruta.tags.all().values())
		for t in tags:
			tag = Tag.objects.get(id=t['id'])
			new_user_route.tags.add(tag)

		"""
		Add categories to the new UserRoute
		"""
		categories = list(ruta.categorias.all().values())
		for c in categories:
			cat = Categoria.objects.get(id=c['id'])
			new_user_route.categorias.add(cat)
		
		return APIResponse("", "Ruta importada", 1)

class RutaUsuarioDeleteView(View):
	def get(self, request, route_id):
		user = CheckToken(request)
		route_id = int(route_id)

		if user is None:
			return APIResponse("", "Token Missing", 0)
		elif user == 0:
			return APIResponse("", "Token incorrect", 0)

		ruta = UsuarioRuta.objects.filter(id=route_id, usuario=user.usuario)
		if ruta.exists():
			ruta = UsuarioRuta.objects.get(id=route_id)
		else:
			return APIResponse("", "La ruta no existe", 0)

		ruta.delete()

		data = {}
		return APIResponse(data, "Ruta eliminada", 1)


class RutaUsuarioEditView(View):
	def get(self, request, route_id):
		user = CheckToken(request)
		route_id = int(route_id)

		if user is None:
			return APIResponse("", "Token Missing", 0)
		elif user == 0:
			return APIResponse("", "Token incorrect", 0)

		ruta = UsuarioRuta.objects.filter(id=route_id)
		if ruta.exists():
			ruta = UsuarioRuta.objects.get(id=route_id)
		else:
			return APIResponse("", "La ruta no existe", 0)
		
		nombre       = request.GET.get('nombre')
		descripcion  = request.GET.get('descripcion')
		usuario   	 = user
		indicaciones = request.GET.get('indicacion')
		transporte   = request.GET.get('transporte')
		compartir 	 = False

		if nombre is None:
			return APIResponse("", "Nombre vacio", 0)

		UsuarioRuta.objects.filter(id=route_id).update(nombre=nombre,descripcion=descripcion, indicacion=indicaciones, transporte=transporte, compartir=compartir)

		data = [{
			"nombre": nombre,
			"descripcion": descripcion,
			"indicacion": indicaciones,
			"transporte": transporte,
			"usuario": ruta.usuario.username,
			"lugares": list(ruta.lugares.all().values()),
			"tags": list(ruta.tags.all().values()),
			"type": "ruta-usuario"
		}]
		return APIResponse(data, "Ruta actualizada", 1)

class RutaUsuarioSingleView(View):
	def get(self, request, route_id):
		user = CheckToken(request)
		route_id = int(route_id)
		idioma = request.idioma
		if user is None:
			return APIResponse("", "Token Missing", 0)
		elif user == 0:
			return APIResponse("", "Token incorrect", 0)

		ruta = UsuarioRuta.objects.filter(id=route_id)
		if ruta.exists():
			ruta = UsuarioRuta.objects.get(id=route_id)
		else:
			return APIResponse("", "La ruta no existe", 0)

		categorias = []
		tags       = list(ruta.tags.all().values())
		x = list(ruta.lugares.all().values())

		for l in x:
			ll = Lugar.objects.get(pk=l['id'])
			if idioma == "EN":
				l['descripcion'] = l['descripcion_ingles']
				l['banner']      = l['banner_ingles']

		# show image
		if ruta.imagen == None:
			ruta_imagen = ""
		else:
			ruta_imagen = settings.DOMINIO + ruta.imagen.url

		indicacion = ruta.indicacion
		transporte = ruta.transporte

		data = {
			"nombre": ruta.nombre,
			"descripcion": ruta.descripcion,
			"indicacion": indicacion,
			"transporte": transporte,
			"usuario": ruta.usuario.username,
			"compartir": ruta.compartir,
			"categorias": categorias,
			"imagen":ruta_imagen,
			"lugares": x,
			"tags": tags,
			"type": "ruta-usuario"
		}
		return APIResponse(data, "Ruta usuario", 1)

class RutaUsuarioAdd(View):
	def get(self, request):
		user = CheckToken(request)
		if user is None:
			return APIResponse("", "Token Missing", 0)
		elif user == 0:
			return APIResponse("", "Token incorrect", 0)

		place_id = request.GET.get('place_id')
		route_id = request.GET.get('route_id')


		if place_id is None or route_id is None:
			return APIResponse("", "Datos vacios", 0)

		rutas = UsuarioRuta.objects.filter(id=route_id)
		if rutas.exists():
			ruta = UsuarioRuta.objects.get(id=route_id)
		else:
			return APIResponse("", "La ruta no existe", 0)

		places = Lugar.objects.filter(id=place_id)
		if places.exists():
			place = Lugar.objects.get(id=place_id)
		else:
			return APIResponse("", "El lugar no existe", 0)

		ruta.lugares.add(place)	

		data = {
			"nombre": ruta.nombre,
			"lugar": place.nombre,
			"type": "ruta"
		}

		return APIResponse(data, "Lugar agregado a la ruta", 1)

class RutaUsuarioRemove(View):
	def get(self, request):
		user = CheckToken(request)
		if user is None:
			return APIResponse("", "Token Missing", 0)
		elif user == 0:
			return APIResponse("", "Token incorrect", 0)

		place_id = request.GET.get('place_id')
		route_id = request.GET.get('route_id')


		if place_id is None or route_id is None:
			return APIResponse("", "Datos vacios", 0)

		rutas = UsuarioRuta.objects.filter(id=route_id)
		if rutas.exists():
			ruta = UsuarioRuta.objects.get(id=route_id)
		else:
			return APIResponse("", "La ruta no existe", 0)

		places = Lugar.objects.filter(id=place_id)
		if places.exists():
			place = Lugar.objects.get(id=place_id)
		else:
			return APIResponse("", "El lugar no existe", 0)

		ruta.lugares.remove(place)	

		data = {
			"nombre": ruta.nombre,
			"lugar": place.nombre,
			"type": "ruta"
		}

		return APIResponse(data, "Lugar removido de la ruta", 1)

class RutaSingleView(View):
	def get(self, request, route_id):
		"""
		Muestra información de una ruta especifica solo pasando el id de la ruta
		"""
		route_id = int(route_id)
		idioma   = request.idioma
		rutas = Ruta.objects.filter(id=route_id)
		if rutas.exists():
			ruta = Ruta.objects.get(id=route_id)
		else:
			return APIResponse("", "La ruta no existe", 0)

		x          = list(ruta.lugares.all().values())
		categorias = list(ruta.categorias.all().values())
		tags       = list(ruta.tags.all().values())

		if idioma == "EN":
			nombre      = ruta.nombre_ingles
			descripcion = ruta.descripcion_ingles
			indicacion  = ruta.indicacion_ingles
		else:
			nombre      = ruta.nombre
			descripcion = ruta.descripcion
			indicacion  = ruta.indicacion

		if ruta.imagen == None:
			ruta_imagen = ""
		else:
			ruta_imagen = settings.DOMINIO + ruta.imagen.url



		for l in x:
			ll = Lugar.objects.get(pk=l['id'])

			if idioma == "EN":
				l['descripcion'] = l['descripcion_ingles']
				l['banner']      = l['banner_ingles']

			p = ll.empresa

			l['imagen'] = ""
			l['logo']   = ""

			if p.logo:
				logo = settings.DOMINIO + p.logo.url
				l['logo'] = logo

			if ll.imagen:
				imagen = settings.DOMINIO + ll.imagen.url
				l['imagen'] = imagen

		data = {
			"nombre": nombre,
			"descripcion": descripcion,
			"lugares": x,
			"categorias": categorias,
			"tags":tags,
			"indicacion": indicacion,
			"transporte": ruta.transporte,
			"imagen": ruta_imagen,
			"type": "ruta"
		}

		return APIResponse(data, "Ruta", 1)

class RutaUsuarioVoteView(View):
	def get(self, request):
		"""
		Permite que los usuarios voten una ruta
		"""

		user        = CheckToken(request)		
		ruta_id     = request.GET.get('ruta_id')
		estrellas   = request.GET.get('estrellas')

		if user is None:
			return APIResponse("", "Token Missing", 0)
		elif user == 0:
			return APIResponse("", "Token incorrect", 0)

		if ruta_id is None:
			if request.idioma == "EN":
				return APIResponse("", "ruta_id can't be empty", 0)
			return APIResponse("", "ruta_id no puede estar vacio", 0)
		elif estrellas is None:
			if request.idioma == "EN":
				return APIResponse("", "estrellas can't be empty", 0)
			return APIResponse("", "estrellas no puede estar vacio", 0)

		if int(estrellas) >= 6:
			if request.idioma == "EN":
				return APIResponse("", "Estrellas must be less or equal to 5", 0)	
			return APIResponse("", "estrellas debe ser menor o igual a 5", 0)


		rutas = Ruta.objects.filter(id=ruta_id)
		if rutas.exists():
			ruta = Ruta.objects.get(id=ruta_id)
		else:
			if request.idioma == "EN":
				return APIResponse("", "The route not exists!", 0)
			return APIResponse("","La ruta no existe!",0)


		voto_usuario = UsuarioRutaVote.objects.filter(usuario=user.usuario, ruta=ruta)
		if voto_usuario.exists():
			voto_usuario.update(estrellas=estrellas)
			total_estrellas = CalcularEstrellasRuta(ruta_id)

		#TENGO QUE PENSAR UN POCO


		#Return
		pass



class LugarView(View):
	def get(self, request):

		s = request.GET.get('s')
		idioma = request.idioma
		if s is not None:			
			data = list(Lugar.objects.filter(Q(nombre__icontains=s) | Q(descripcion__icontains=s)).values())
		else:
			data = list(Lugar.objects.filter(expiracion__gte=datetime.datetime.now()).values())
			#data = list(Lugar.objects.filter(expiracion__gte=datetime.datetime.now()).values())

		for d in data:
			o = Empresa.objects.filter(pk=d['empresa_id'])
			p = o.first()

			if idioma == "EN":
				d['descripcion'] = d['descripcion_ingles']
				d['banner']      = d['banner_ingles']

			if p.categoria:
				cat = Categoria.objects.get(pk=p.categoria.id)
				d['categorias'] = cat.nombre
			else:
				d['categorias'] = ""

			d['imagen'] = settings.DOMINIO + 'media/' + d['imagen']

			d['empresa'] = list(o.values())

			if p.logo:
				d['logo'] = settings.DOMINIO + p.logo.url
			else:
				d['logo'] = ""

			lugar = Lugar.objects.get(id=d['id'])

			d['estrellas'] = estrellas = CalcularEstrellasLugar(lugar.id)

			token_ = request.GET.get('token')
			if token_ is not None:
				
				user = CheckToken(request)

				voto_usuario = UsuarioLugar.objects.filter(usuario=user.usuario, lugar=lugar)
				if voto_usuario.exists():
					voto = voto_usuario.first()
					d['estrellas_usuario'] = voto.estrellas

		return	APIResponse(data, "Lugar", 1)

class LugarSingleView(View):
	def get(self, request, lugar_id):
		"""
		Vista para obtener los datos de un lugar | recibe el id del lugar
		"""
		lugar_id = int(lugar_id)
		lugar = Lugar.objects.filter(pk=lugar_id)
		idioma = request.idioma

		if lugar.exists():
			lugar = Lugar.objects.get(pk=lugar_id)
		else:
			return APIResponse("", "El lugar no existe", 0)

		imagen = ""
		if lugar.imagen:
			imagen = settings.DOMINIO + lugar.imagen.url

		estrellas = CalcularEstrellasLugar(lugar_id)
		estrellas_usuario = None

		token_ = request.GET.get('token')
		if token_ is not None:
				
				user = CheckToken(request)

				voto_usuario = UsuarioLugar.objects.filter(usuario=user.usuario, lugar=lugar)
				if voto_usuario.exists():
					voto = voto_usuario.first()
					estrellas_usuario = voto.estrellas

		if idioma == "EN":
			banner      = lugar.banner_ingles
			descripcion = lugar.descripcion_ingles
		else:
			banner      = lugar.banner
			descripcion = lugar.descripcion

		data = {
			"id": lugar.id,
			"nombre":lugar.nombre,
			"imagen": imagen,
			"lat": lugar.lat,
			"lon": lugar.lon,
			"descripcion": descripcion,
			"descripcion_ingles": lugar.descripcion_ingles,
			"hora_entrada": lugar.hora_entrada,
			"hora_salir": lugar.hora_salir,
			"direccion": lugar.direccion,
			"telefono": lugar.telefonos,
			"verificado": lugar.esta_verificado(),
			"estrellas": estrellas,
			"estrellas_usuario": estrellas_usuario,
			"banner": banner,
			"pin": lugar.categoria.color
		}

		return APIResponse(data, "Single Lugar", 1)

class UsuarioLugarView(View):
	def get(self, request):
		"""
		Vista que permite a los usuarios votar los lugares del 1 al 5.
		Recibe: lugar_id: int & estrellas: int >= 0 & <=5
		"""
		user         = CheckToken(request)		
		lugar_id     = request.GET.get('lugar_id')
		estrellas    = request.GET.get('estrellas')

		if user is None:
			return APIResponse("", "Token Missing", 0)
		elif user == 0:
			return APIResponse("", "Token incorrect", 0)

		if lugar_id is None:
			if request.idioma == "EN":
				return APIResponse("", "lugar_id can't be empty", 0)
			return APIResponse("", "lugar_id no puede estar vacio", 0)
		elif estrellas is None:
			if request.idioma == "EN":
				return APIResponse("", "estrellas can't be empty", 0)
			return APIResponse("", "estrellas no puede estar vacio", 0)

		if int(estrellas) >= 6:
			if request.idioma == "EN":
				return APIResponse("", "Estrellas must be less or equal to 5", 0)	
			return APIResponse("", "estrellas debe ser menor o igual a 5", 0)

		lugares = Lugar.objects.filter(id=lugar_id)
		if lugares.exists():
			lugar = Lugar.objects.get(id=lugar_id)
		else:
			if request.idioma == "EN":
				return APIResponse("", "The place not exists!", 0)	
			return APIResponse("", "¡El lugar no existe!", 0)
		voto_usuario = UsuarioLugar.objects.filter(usuario=user.usuario, lugar=lugar)
		if voto_usuario.exists():
			voto_usuario.update(estrellas=estrellas)
			total_estrellas = CalcularEstrellasLugar(lugar_id)
			Lugar.objects.filter(id=lugar_id).update(estrellas=total_estrellas)
			data = {
				"lugar_id": lugar.id,
				"estrellas": estrellas,
				"type":"usuario-lugar"
			}
			if request.idioma == "EN":
				return APIResponse(data, "Vote was updated!", 1)	
			return APIResponse(data, "Voto actualizado", 1)

		votoUsuario = UsuarioLugar()
		votoUsuario.usuario = user.usuario
		votoUsuario.estrellas = estrellas
		votoUsuario.lugar = lugar
		votoUsuario.save()

		total_estrellas = CalcularEstrellasLugar(lugar_id)
		Lugar.objects.filter(id=lugar_id).update(estrellas=total_estrellas)

		data = {
			"lugar_id": votoUsuario.lugar.id,
			"estrellas": estrellas,
			"type":"usuario-lugar"
		}

		if request.idioma == "EN":
			return APIResponse(data, "Place rated!", 1)	
		return APIResponse(data, "Voto agregado", 1)

class CategoriaView(View):
	def get(self, request):
		data = list(Categoria.objects.all().values())
		user = CheckToken(request)
		idioma = request.GET.get('lang')

		for d in data:
			"""
			Funcion para calcular el numero de estrellas gracias a los votos de usuarios
			"""
			categoria = Categoria.objects.get(pk=d['id'])
			votos_usuarios = UsuarioCategoria.objects.filter(categoria=categoria)
			votos = 0
			for voto in votos_usuarios:
				votos = votos + voto.estrellas
			try:
				estrellas = (int(votos) / int(votos_usuarios.count()))
			except ZeroDivisionError, e:
				estrellas = 0
			d['estrellas'] = estrellas

			if idioma == "EN":
				d['nombre'] = d['nombre_ingles']

			"""
			Funcion para detectar si el usuario ya voto la categoria
			"""
			d['voted'] = False
			if user is not None:
				voto_usuario = UsuarioCategoria.objects.filter(usuario=user.usuario, categoria=categoria)
				if voto_usuario.exists():
					d['voted'] = True

		return APIResponse(data, "CategoriaView", 1)

class UsuarioCategoriaView(View):
	def get(self, request):
		"""
		Vista que permite a los usuarios votar las categorias del 1 al 5.
		Recibe: categoria_id: int & estrellas: int >= 0 & <=5
		Ejemplo:
		{
			"categoria_id": 3,
			"estrellas": 5
		}
		"""
		user = CheckToken(request)
		#json data from request
		#post_request = json.loads(request.body)
		categoria_id = request.GET.get('categoria_id')
		estrellas = request.GET.get('estrellas')

		if estrellas > 5:
			estrellas = 5

		if user is None:
			return APIResponse("", "Token Missing", 0)
		elif user == 0:
			return APIResponse("", "Token incorrect", 0)

		if categoria_id is None or estrellas is None:
			return APIResponse("", "Los campos no pueden estar vacios", 0)
		cat = Categoria.objects.get(pk=categoria_id)

		data = {
			"type": "usuario-categoria",
			"categoria": categoria_id,
			"estrellas": estrellas
		}

		"""
		si se vota con 3 o mas estrellas se agrega al perfil un interes "tag" con el
		mismo nombre de la categoria
		"""

		if estrellas >= 3:
			tags = Tag.objects.filter(nombre=cat.nombre)
			tag = None

			if tags.exists():
				tag = Tag.objects.get(nombre=cat.nombre)			
			else:
				tag = Tag()
				tag.nombre = cat.nombre
				tag.save()

			perfil = user
			perfil.intereses.add(tag)


		voto_usuario = UsuarioCategoria.objects.filter(usuario=user.usuario, categoria=cat)
		if voto_usuario.exists():
			voto_usuario.update(estrellas=estrellas)
			votos_usuarios = UsuarioCategoria.objects.filter(categoria=cat)
			votos = 0
			for voto in votos_usuarios:
				votos = votos + voto.estrellas
			try:
				estrellas = (int(votos) / int(votos_usuarios.count()))
			except ZeroDivisionError, e:
				estrellas = 0

			cat2 = Categoria.objects.filter(id=categoria_id)
			if cat2.exists():
				cat2.update(estrellas=estrellas)
			return APIResponse(data, "Voto actualizado", 1)

		uscat = UsuarioCategoria()
		uscat.categoria = cat
		uscat.usuario = user.usuario
		uscat.estrellas = estrellas
		uscat.save()


		votos_usuarios = UsuarioCategoria.objects.filter(categoria=cat)
		votos = 0
		for voto in votos_usuarios:
			votos = votos + voto.estrellas
		try:
			estrellas = (int(votos) / int(votos_usuarios.count()))
		except ZeroDivisionError, e:
			estrellas = 0

		cat2 = Categoria.objects.filter(id=categoria_id)
		if cat2.exists():
			cat2.update(estrellas=estrellas)

		return APIResponse(data, "Voto agregado", 1)

class LoginView(View):
	def get(self, request):
		#json data from request
		#post_request = json.loads(request.body)

		username = request.GET.get('usuario')
		contrasena = request.GET.get('contrasena')

		if username is None or contrasena is None:
			if request.idioma == "EN":
				return APIResponse("", "usuario/contrasena is empty!", 0)	
			return APIResponse("", "No se permiten campos vacios", 0)

		user = authenticate(username=username, password=contrasena)
		if user is not None:
			if user.is_active:
				tok = uuid.uuid4()
				userInfo = UsuarioInformacion.objects.filter(token=tok)
				while userInfo.exists():
					tok = uuid.uuid4()
					userInfo = UsuarioInformacion.objects.filter(token=tok)

				UsuarioInformacion.objects.filter(usuario=user).update(token=tok)
				useri = UsuarioInformacion.objects.get(usuario=user)

				foto = ''
				if useri.foto:
					#image = urllib.urlopen(settings.DOMINIO + useri.foto.url)
					#foto = "data:image/jpeg;base64,%s"%base64.encodestring(image.read()).replace("\n","")
					foto = settings.DOMINIO + useri.foto.url

				user_json = [{
					"type": "user",
					"username": user.username,
					"user_id": user.id,
					"email": user.email,
					"foto": foto,
					"idioma": useri.idioma,
					"token": str(tok).replace('-','')
				}]
				return APIResponse(user_json, "Login", 1)
			if request.idioma == "EN":
				return APIResponse("", "User inactive!", 0)
			return APIResponse("", "Usuario inactivo", 0)
		if request.idioma == "EN":
			return APIResponse("", "Usuario/Contraseña wrong!", 0)
		return APIResponse("", "Usuario/Contraseña incorrecta", 0)

"""
Social login vanilla
"""

class AuthFacebookView(View):	
	def post(self, request):
		LugarSingleViewnombre           = request.POST.get('name')
		edad             = request.POST.get('age')
		genero           = request.POST.get('gender')
		pais             = request.POST.get('country')
		ciudad           = request.POST.get('city')
		relacion         = request.POST.get('relationship')
		likes            = request.POST.get('likes')
		fecha_nacimiento = request.POST.get('birthday')
		email            = request.POST.get('email')
		idioma           = request.POST.get('language')
		usuario          = request.POST.get('username')
		facebook_token   = request.POST.get('facebook_token')

		if usuario is None:
			if request.idioma == "EN":
				return APIResponse("", "username is empty", 0)
			else:
				return APIResponse("", "username no puede estar vacio", 0)

		if nombre is None:
			if request.idioma == "EN":
				return APIResponse("", "Name is empty", 0)
			else:
				return APIResponse("", "Name no puede estar vacio", 0)
		elif edad is None:
			if request.idioma == "EN":
				return APIResponse("", "Age is empty", 0)
			else:
				return APIResponse("", "Age no puede estar vacio", 0)
		elif facebook_token is None:
			if request.idioma == "EN":
				return APIResponse("", "facebook_token is empty", 0)
			else:
				return APIResponse("", "facebook_token no puede estar vacio", 0)
		elif pais is None:
			if request.idioma == "EN":
				return APIResponse("", "Country is empty", 0)
			else:
				return APIResponse("", "Country no puede estar vacio", 0)
		elif ciudad is None:
			if request.idioma == "EN":
				return APIResponse("", "City is empty", 0)
			else:
				return APIResponse("", "City no puede estar vacio", 0)
		elif relacion is None:
			if request.idioma == "EN":
				return APIResponse("", "Relationship is empty", 0)
			else:
				return APIResponse("", "Relationship no puede estar vacio", 0)
		elif likes is None:
			if request.idioma == "EN":
				return APIResponse("", "User facebook likes is empty", 0)
			else:
				return APIResponse("", "Likes no puede estar vacio", 0)
		elif fecha_nacimiento is None:
			if request.idioma == "EN":
				return APIResponse("", "Birthday is empty", 0)
			else:
				return APIResponse("", "Birthday no puede estar vacio", 0)
		elif email is None:
			if request.idioma == "EN":
				return APIResponse("", "Email is empty", 0)
			else:
				return APIResponse("", "Email no puede estar vacio", 0)

		# if not pass any language so will be english
		if idioma is None:
			idioma = "en"
		elif idioma != "EN" or idioma != "ES" or idioma != "es" or idioma != "en":
			idioma = "en"
		elif idioma == "EN" or idioma == "en":
			idioma = "en"
		elif idioma == "ES" or idioma == "es":
			idioma = "es"

		# convert cumpleanos in date object
		cumpleanos_cleaned = datetime.datetime.strptime(fecha_nacimiento, "%Y-%m-%d")

		usuarios = UsuarioInformacion.objects.filter(facebook_token=facebook_token)
		#If not exists then create the user
		if not usuarios.exists():
			user                     = User.objects.create_user(usuario, email, "!W4nd3rc00n$!")
			user.first_name          = nombre
			user.is_active           = True
			user.save()
			userInfo                 = UsuarioInformacion.objects.get(usuario=user)
			userInfo.pais            = pais
			userInfo.ciudad          = ciudad
			userInfo.idioma          = idioma
			userInfo.nacimiento      = cumpleanos_cleaned
			userInfo.genero          = genero
			userInfo.edad            = edad
			userInfo.estado_relacion = relacion
			userInfo.facebook_likes  = likes
			userInfo.facebook_token  = facebook_token
			tok                      = uuid.uuid4()
			userInfo_                = UsuarioInformacion.objects.filter(token=tok)

			while userInfo_.exists():
				tok = uuid.uuid4()
				userInfo_ = UsuarioInformacion.objects.filter(token=tok)

			userInfo.token = tok
			userInfo.save()

			data = [{
					"type": "user",
					"username": user.username,
					"user_id": user.id,
					"email": user.email,					
					"idioma": userInfo.idioma,
					"token": str(tok).replace('-','')
			}]
			if request.idioma == "EN":
				return APIResponse(data, "User registered", 1)
			else:
				return APIResponse(data, "Usuario registrado", 1)
		#If exists in databse
		else:
			tok = uuid.uuid4()
			userInfo = UsuarioInformacion.objects.filter(token=tok)
			while userInfo.exists():
				tok = uuid.uuid4()
				userInfo = UsuarioInformacion.objects.filter(token=tok)

			UsuarioInformacion.objects.filter(facebook_token=facebook_token).update(token=tok)
			useri = UsuarioInformacion.objects.get(facebook_token=facebook_token)
			user  = useri.usuario

			foto = ''
			if useri.foto:				
				foto = settings.DOMINIO + useri.foto.url

			data = [{
					"type": "user",
					"username": user.username,
					"user_id": user.id,
					"email": user.email,					
					"idioma": useri.idioma,
					"token": str(tok).replace('-','')
			}]

			if request.idioma == "EN":
				return APIResponse(data, "Facebook login successful", 1)
			else:
				return APIResponse(data, "Logeado correctamente", 1)
			
		return APIResponse("", "ERROR FACEBOOK LOGIN", 0)

class RegistroView(View):
	def get(self, request):
		"""
		Registro desde la aplicacion movil.
		Ejemplo de registro JSON:
		{
			"usuario": "PruebaAPI",
			"contrasena":"25262782k",
			"re_contrasena":"25262782k",
			"email": "info@kildarelauser.me",
			"nombre": "PruebaAPI",
			"apellido": "Prueba",
			"pais": "VE",
			"idioma": "en",
			"cumpleanos": "21-10-1998"
		}
		"""
		#json data from request
		#post_request = json.loads(request.body)

		username      = request.GET.get('usuario')
		contrasena    = request.GET.get('contrasena')
		re_contrasena = request.GET.get('re_contrasena')
		email         = request.GET.get('email')
		nombre        = request.GET.get('nombre')
		pais          = request.GET.get('pais')
		idioma        = request.GET.get('idioma')
		cumpleanos    = request.GET.get('cumpleanos')
		#foto          = request.GET.get('profile_pic')

		if username is None or contrasena is None or re_contrasena is None or email is None:
			return APIResponse("", "No se permiten campos vacios", 0)
		elif nombre is None or nombre == "":
			return APIResponse("", "No se permiten campos vacios", 0)
		elif contrasena != re_contrasena:
			return APIResponse("", "Las contraseñas no coinciden", 0)
		elif idioma != "es" and idioma != "en" and idioma != "ES" and idioma != "EN":
			return APIResponse("", "Idioma incorrecto. (EN || ES)", 0)
		elif cumpleanos is None or cumpleanos == "":
			return APIResponse("", "Debe de ingresar tu cumpleaños", 0)
		#elif foto is None:
			#return APIResponse("", "profile_pic no puede estar vacio", 0)

		user = User.objects.filter(username=username)
		if user.exists():
			return APIResponse("", "Usuario existente", 0)
		user = User.objects.filter(email=email)
		if user.exists():
			return APIResponse("", "E-mail en uso", 0)

		cumpleanos_cleaned = datetime.datetime.strptime(cumpleanos, "%Y-%m-%d")

		user                = User.objects.create_user(username, email, contrasena)
		user.first_name     = nombre
		user.is_active      = True
		user.save()
		userInfo            = UsuarioInformacion.objects.get(usuario=user)
		userInfo.pais       = pais
		userInfo.idioma     = idioma
		userInfo.nacimiento = cumpleanos_cleaned
		tok                 = uuid.uuid4()
		userInfo_           = UsuarioInformacion.objects.filter(token=tok)

		while userInfo_.exists():
			tok = uuid.uuid4()
			userInfo_ = UsuarioInformacion.objects.filter(token=tok)

		userInfo.token = tok

		#foto_data = b64decode(foto)
		#userInfo.foto = ContentFile(foto_data, 'user.jpg')
		userInfo.save()

		response_ctx = {
			'user_id': user.id,
			'usuario': user.username,
			'email': user.email,
			'nombre': user.first_name,
			'token': str(tok).replace("-",""),
			'cumpleanos': cumpleanos,
			'type': 'user'
		}
		return APIResponse(response_ctx, "Usuario creado", 1)

class RecuperarView(View):
	def get(self, request):
		token = request.GET.get('token')
		if token is None:
			return APIResponse("", "Token missing", 0)
		user = UsuarioInformacion.objects.filter(token_rest=token)
		if not user.exists():
			messages.error(request, 'Token Invalido!')
		ctx = {
			'user':user[0]
		}
		return render(request, 'email/restaurar_contrasena.html', ctx)

	def post(self, request):
		token = request.GET.get('token')
		email_ = request.POST.get('email')
		if token is None:
			if email_ is None:
				return APIResponse("", "Email vacio", 0)
			user = User.objects.filter(email=email_)
			if not user.exists():
				return APIResponse("", "No existe cuenta vinculada", 0)
			user = user.first()
			tok = uuid.uuid4()
			userInfo = UsuarioInformacion.objects.filter(token_rest=tok)
			while userInfo.exists():
				tok = uuid.uuid4()
				userInfo = UsuarioInformacion.objects.filter(token_rest=tok)

			UsuarioInformacion.objects.filter(usuario=user).update(token_rest=tok)
			url_rest = settings.DOMINIO + 'api/restaurar/?token=' + str(tok).replace("-","")
			html_message = '<strong>Hola ' + user.username + '!</strong><br /><br />Se ha solicitado hacer una restauracion de tu clave, para hacerla puedes hacer click en el link a continuacion.<br /<br /><a href="' + url_rest + '">Cambiar Clave</a><br /><br />Si tu no hiciste esta peticion, solo ignora este mensaje.'
			send_m = EmailMessage('Restaurar Contraseña', html_message, settings.EMAIL_CONTACT, [email_])
			send_m.content_subtype = "html"
			send_m.send()
			return JsonResponse({'success': 'true'})
		else:
			user = UsuarioInformacion.objects.filter(token_rest=token)
			if user.exists():
				contrasena = request.POST.get('contrasena')
				re_contrasena = request.POST.get('re_contrasena')
				err = False
				if contrasena != re_contrasena:
					messages.error(request, 'Las contraseñas no coinciden')
					err = True
				if contrasena is None or contrasena == '':
					messages.error(request, 'No se permiten contraseñas vacias')
					err = True

				if not err:
					user = user.first().usuario
					user.set_password(contrasena)
					user.save()
					return render(request, 'email/restaurar_contrasena.html', {"success": True})
					#messages.success(request, 'Contraseña Cambiada Exitosamente!')
			else:
				return APIResponse("", "Token invalido", 0)
				#messages.error(request, 'Token Invalido!')
			return render(request, 'email/restaurar_contrasena.html')

class OlvidoView(View):
	def get(self, request):
		email_ = request.GET.get('email')
		if email_ is None:
			if request.idioma == "EN":
				return APIResponse("", "email is empty!", 0)	
			return APIResponse("", "Debe de especificar un correo", 0)

		user = User.objects.filter(email=email_)
		if not user.exists():
			if request.idioma == "EN":
				return APIResponse("", "Account doesn't exists!", 0)	
			return APIResponse("", "No existe cuenta vinculada", 0)
		user = user.first()
		tok = uuid.uuid4()
		userInfo = UsuarioInformacion.objects.filter(token_rest=tok)
		while userInfo.exists():
			tok = uuid.uuid4()
			userInfo = UsuarioInformacion.objects.filter(token_rest=tok)

		UsuarioInformacion.objects.filter(usuario=user).update(token_rest=tok)
		url_rest = settings.DOMINIO + 'api/restaurar/?token=' + str(tok).replace("-","")
		html_message = '<strong>Hola ' + user.username + '!</strong><br /><br />Se ha solicitado hacer una restauracion de tu clave, para hacerla puedes hacer click en el link a continuacion.<br /<br /><a href="' + url_rest + '">Cambiar Clave</a><br /><br />Si tu no hiciste esta peticion, solo ignora este mensaje.'
		send_m = EmailMessage('Restaurar Contraseña', html_message, settings.EMAIL_CONTACT, [email_])
		send_m.content_subtype = "html"		
		try:
			send_m.send()
		except smtplib.SMTPException,error:
			if request.idioma == "EN":
				return APIResponse("", "Error trying send the email", 0)	
			return APIResponse("", "Error al enviar correo", 0)
		if request.idioma == "EN":
			return APIResponse("", "Email already send!", 1)	
		return APIResponse("", "Email de recuperación enviado", 1)

class ProfileView(View):
	"""
	Vista que permite visualizar los datos del usuario logeado
	y hacer update a su Informacion
	"""
	def get(self, request):
		user = CheckToken(request)

		if request.GET.get('token') is None:
			return APIResponse("", "Token missing", 0)

		if user is None:
			return APIResponse("", "Token incorrect", 0)

		if user.foto == None or user.foto == "":
			profile_pic = ""
		else:
			profile_pic = settings.DOMINIO + settings.MEDIA_URL + str(user.foto)

		usuario = [{
			"usuario": user.usuario.username,
			"nombre": user.usuario.first_name,
			"about_me": user.about_me,
			"foto": profile_pic,
			"email": user.usuario.email,
			"tipo": user.tipo,
			#"ciudad": user.ciudad,
			"pais": user.pais,
			"idioma":user.idioma,
			"intereses":list(user.intereses.all().values()),
			"cumpleanos": user.nacimiento,
			"type": "user"
		}]

		return APIResponse(usuario, "User profile", 1)

class ProfileEditView(View):
	def get(self, request):
		"""
		API para editar perfil de los usuarios		
		"""
		user = CheckToken(request)

		if user is None:
			return APIResponse("", "Token Missing", 0)
		elif user == 0:
			return APIResponse("", "Token incorrect", 0)

		nombre     = request.GET.get('nombre')
		about_me   = request.GET.get('about-me')
		ciudad     = request.GET.get('ciudad')
		pais       = request.GET.get('pais')
		idioma     = request.GET.get('idioma')
		cumpleanos = request.GET.get('cumpleanos')

		if nombre is None is None or pais is None or idioma is None:
			if request.idioma == "EN":
				return APIResponse("", "Some param is empty", 0)	
			return APIResponse("", "Campos vacios", 0)
		elif cumpleanos is None or cumpleanos == "":
			if request.idioma == "EN":
				return APIResponse("", "cumpleanos can't be empty", 0)	
			return APIResponse("", "Debe de ingresar tu cumpleaños", 0)

		if about_me == None or about_me == "":
			about_me = ""

		cumpleanos_cleaned = datetime.datetime.strptime(cumpleanos, "%Y-%m-%d")

		usuario  = User.objects.get(username=user.usuario.username)
		usuario.first_name  = nombre
		usuario.save()
		userInfo = UsuarioInformacion.objects.get(usuario=user.usuario)
		#userInfo.ciudad     = ciudad
		userInfo.about_me   = about_me
		userInfo.pais       = pais
		userInfo.idioma     = idioma
		userInfo.nacimiento = cumpleanos_cleaned
		userInfo.save()

		usuario = [{
			"usuario": user.usuario.username,
			"nombre": nombre,
			"about_me": about_me,
			"foto": settings.DOMINIO + settings.MEDIA_URL + str(user.foto),
			"tipo": user.tipo,
			#"ciudad": ciudad,
			"pais": pais,
			"idioma":idioma,
			"cumpleanos": cumpleanos,
			"intereses":list(user.intereses.all().values()),
			"type": "user"
		}]

		if request.idioma == "EN":
			return APIResponse(usuario, "Profile info updated!", 1)

		return APIResponse(usuario, "Informacion actualizada", 1)

class PhotoProfileUploadView(View):
	def post(self, request):
		user = CheckToken(request)
		if user is None:
			return APIResponse("", "Token Missing", 0)
		elif user == 0:
			return APIResponse("", "Token incorrect", 0)	

		#print request.body
		if request.FILES.get('foto') is None:
			return APIResponse("", "Photo missing", 0)

		user.foto = request.FILES.get('foto')
		user.save()

		userInfo = CheckToken(request)

		data = {
			"foto": settings.DOMINIO + settings.MEDIA_URL + str(userInfo.foto),
		}

		return APIResponse(data, "Photo uploaded", 1)

class TagView(View):
	def get(self, request):
		tags = list(Tag.objects.all().values())

		return APIResponse(tags, "Tag", 1)

class ChangePasswordView(View):
	def get(self, request):
		user = CheckToken(request)

		if user is None:
			return APIResponse("", "Token Missing", 0)
		elif user == 0:
			return APIResponse("", "Token incorrect", 0)

		#json data from request
		#post_request = json.loads(request.body)

		contrasena     = request.GET.get('contrasena')
		re_contrasena  = request.GET.get('re_contrasena')
		old_contrasena = request.GET.get('old_contrasena')
		usuario        = user.usuario

		if contrasena is None or re_contrasena is None:
			return APIResponse("", "Contrasena vacia", 0)

		if contrasena != re_contrasena:
			return APIResponse("", "Las Contrasenas no coinciden", 0)

		if not usuario.check_password(old_contrasena):
			return APIResponse("", "Antigua contraseña incorrecta", 0)

		# Cambiar contraseña
		usuario.set_password(contrasena)
		usuario.save()

		return APIResponse("", "Contraseña cambiada", 1)

class UsuarioAddTag(View):
	def get(self, request):
		"""
		Permite agregar tags al perfil del usuario indicado en el token
		"""
		user = CheckToken(request)
		if user is None:
			return APIResponse("", "Token Missing", 0)
		elif user == 0:
			return APIResponse("", "Token incorrect", 0)

		tag_nombre   = request.GET.get('nombre')
		userInfo     = user

		if tag_nombre is None:
			return APIResponse("", "Campos vacios", 0)

		tag = Tag.objects.filter(nombre=tag_nombre)
		if tag.exists():
			tag = Tag.objects.get(nombre=tag_nombre)
		else:
			tag = Tag()
			tag.nombre = tag_nombre
			tag.save()

		userInfo.intereses.add(tag)

		usuario = {
			"tag": tag_nombre
		}
		return APIResponse(usuario, "Tag agregado al perfil", 1)



class SuggestedRoutesView(View):
	def get(self, request):
		user = CheckToken(request)
		if user is None:
			return APIResponse("", "Token Missing", 0)
		elif user == 0:
			return APIResponse("", "Token incorrect", 0)

		idioma = request.GET.get('lang')
		intereses_usuario = user.intereses.all()
		cantidad = len(list(intereses_usuario.values()))
		sugeridos = []
		contador = 0

		for interes in intereses_usuario:
			if cantidad == 0:
				sugeridos = []

			if cantidad == 1:
				lugares = Lugar.objects.filter(tags=interes).order_by('-estrellas')
				if lugares.exists():
					for l in lugares.values()[0:3]:					
						data = {
							"nombre": l['nombre'],
							"estrellas": l['estrellas'],
							"id": l['id']
						}
						sugeridos.append(data)

			elif cantidad > 6:
				if contador < 6:
					lugares = Lugar.objects.filter(tags=interes).order_by('-estrellas').values().first()
					if lugares is not None:
						data = {
							"nombre": lugares['nombre'],
							"estrellas": lugares['estrellas'],
							"id": lugares['id']
						}
						sugeridos.append(data)
					contador = contador + 1

			else:
				lugares = Lugar.objects.filter(tags=interes).order_by('-estrellas').values().first()
				if lugares is not None:
					data = {
						"nombre": lugares['nombre'],
						"estrellas": lugares['estrellas'],
						"id": lugares['id']
					}
					sugeridos.append(data)		

		data = {
			"lugares": sugeridos
		}

		return APIResponse(data, "suggested-routes", 1)		


class RouteListNewView(View):
	def get(self, request):
		"""
		Este api muestra las rutas sugeridad y las rutas del usuario
		"""
		user = CheckToken(request)
		if user is None:
			return APIResponse("", "Token Missing", 0)
		elif user == 0:
			return APIResponse("", "Token incorrect", 0)

		#START SUGGESTED ROUTES
		categorias_valoradas = Categoria.objects.all().order_by("-estrellas")
		intereses_usuario    = user.intereses.all()
		cantidad             = len(list(intereses_usuario.values()))
		sugeridos            = []
		contador             = 0
		
		if cantidad == 0:
			for num in range(0,3):
				rutas = Ruta.objects.filter(categorias=categorias_valoradas[contador])
				if rutas.exists():
					for r in rutas.values()[0:1]:				
						data = {
							"nombre": r['nombre'],						
							"id": r['id']
						}
						sugeridos.append(data)
				contador = contador + 1

		for interes in intereses_usuario:
			if cantidad == 1:
				rutas = Ruta.objects.filter(tags=interes)
				if rutas.exists():
					for r in rutas.values()[0:3]:
						data = {
							"nombre": r['nombre'],						
							"id": r['id']
						}
						sugeridos.append(data)

			elif cantidad > 6:
				if contador < 6:
					rutas = Ruta.objects.filter(tags=interes).values().first()
					if rutas is not None:
						data = {
							"nombre": rutas['nombre'],						
							"id": rutas['id']
						}
						sugeridos.append(data)
					contador = contador + 1

			else:
				rutas = Ruta.objects.filter(tags=interes).values().first()
				if rutas is not None:
					data = {
						"nombre": rutas['nombre'],						
						"id": rutas['id']
					}
					sugeridos.append(data)
				contador = contador + 1

		if len(sugeridos) < 3 and len(sugeridos) >= 1:
			for num in range(0,2):
				rutas = Ruta.objects.filter(categorias=categorias_valoradas[contador])
				if rutas.exists():
					for r in rutas.values()[0:1]:				
						data = {
							"nombre": r['nombre'],						
							"id": r['id']
						}
						sugeridos.append(data)
						contador += 1

		# START USER ROUTES
		rutas = UsuarioRuta.objects.filter(usuario=user.usuario)
			
		if not rutas:
			success = 0
		else:
			success = 1

		rutas = list(rutas.values())

		for ruta in rutas:
			r = UsuarioRuta.objects.get(pk=ruta['id'])
			# Rutas
			lugares = list(r.lugares.all().values())
			ruta['lugares'] = lugares
			# tags
			tags = list(r.tags.all().values())
			ruta['tags'] = tags

		data = [{
			"suggested": sugeridos,
			"my-routes": rutas
		}]

		return APIResponse(data, "route-list", 1)


class InterestSelectView(View):
	def get(self, request):
		categorias = list(Categoria.objects.all().order_by("id")[0:6].values())

		data = {
			"intereses": categorias
		}

		return APIResponse(data, "interest-list", 1)

class InterestVotes(View):
	def get(self, request):
		cat0 = request.GET.get('Shopping')
		cat1 = request.GET.get('Cultural')
		cat2 = request.GET.get('Adventure')
		cat3 = request.GET.get('Foodie')
		cat4 = request.GET.get('Lodging')
		cat5 = request.GET.get('Nightlife')

		user = CheckToken(request)
		if user is None:
			return APIResponse("", "Token Missing", 0)
		elif user == 0:
			return APIResponse("", "Token incorrect", 0)
		elif cat0 is None or cat1 is None or cat2 is None or cat3 is None or cat4 is None or cat5 is None:
			if request.idioma == "EN":
				return APIResponse("", "Fields empty", 0)	
			return APIResponse("", "Campos vacios", 0)

		for x in range(0, 6):
			if x == 0:
				cat = Categoria.objects.get(nombre_ingles="Shopping")
				if cat0 is None:
					estrellas = 0
				else:
					estrellas = cat0					
			elif x == 1:
				cat = Categoria.objects.get(nombre_ingles="Cultural")
				if cat1 is None:
					estrellas = 0
				else:			
					estrellas = cat1
			elif x == 2:
				cat = Categoria.objects.get(nombre_ingles="Adventure")
				if cat2 is None:
					estrellas = 0
				else:			
					estrellas = cat2
			elif x == 3:
				cat = Categoria.objects.get(nombre_ingles="Foodie")
				if cat3 is None:
					estrellas = 0
				else:			
					estrellas = cat3
			elif x == 4:
				cat = Categoria.objects.get(nombre_ingles="Lodging")
				if cat4 is None:
					estrellas = 0
				else:			
					estrellas = cat4
			elif x == 5:
				cat = Categoria.objects.get(nombre_ingles="Nightlife")
				if cat5 is None:
					estrellas = 0
				else:			
					estrellas = cat5

			voto_usuario = UsuarioCategoria.objects.filter(usuario=user.usuario, categoria=cat)
			if voto_usuario.exists():
				voto_usuario.update(estrellas=estrellas)
			else:
				uscat = UsuarioCategoria()
				uscat.categoria = cat
				uscat.usuario = user.usuario
				uscat.estrellas = estrellas
				uscat.save()					

			"""
			si se vota con 3 o mas estrellas se agrega al perfil un interes "tag" con el
			mismo nombre de la categoria
			"""
			if int(estrellas) >= 3:
				tags = Tag.objects.filter(nombre=cat.nombre_ingles)
				tag = None
				if tags.exists():
					tag = Tag.objects.get(nombre=cat.nombre_ingles)			
				else:
					tag = Tag()
					tag.nombre = cat.nombre_ingles
					tag.save()

				perfil = user
				perfil.intereses.add(tag)					
			

		if request.idioma == "EN":
			return APIResponse("", "Votes saved!", 1)

		return APIResponse("", "Votos agregados correctamente", 1)


class UsuarioRutaUploadImagen(View):
	def post(self, request, route_id):
		user = CheckToken(request)
		if user is None:
			return APIResponse("", "Token Missing", 0)
		elif user == 0:
			return APIResponse("", "Token incorrect", 0)	

		#print request.body
		if request.FILES.get('imagen') is None:
			return APIResponse("", "Imagen is missing", 0)

		rutas = UsuarioRuta.objects.filter(id=route_id)
		if rutas.exists():
			ruta = UsuarioRuta.objects.get(id=route_id)
		else:
			if request.idioma == "EN":
				return APIResponse("", "Ruta not exists!", 0)
			else:
				return APIResponse("", "¡Ruta inexistente!", 0)

		ruta.imagen = request.FILES.get('imagen')
		ruta.save()		

		data = {
			"imagen": settings.DOMINIO + settings.MEDIA_URL + str(ruta.imagen),
		}

		if request.idioma == "EN":
			return APIResponse(data, "Imagen was uploaded", 1)
		else:
			return APIResponse(data, "Imagen subida correctamente!", 1)


"""
Social login with Python Social Auth (NOT USING)
"""
@psa('social:complete')
def register_by_access_token(request, backend):
	token = request.GET.get('access_token')
	if token is None:
		return APIResponse("", "Token missing", 0)
	user = request.backend.do_auth(request.GET.get('access_token'))
	if user is not None:
		if user.is_active:
			tok = uuid.uuid4()
			userInfo = UsuarioInformacion.objects.filter(token=tok)
			while userInfo.exists():
				tok = uuid.uuid4()
				userInfo = UsuarioInformacion.objects.filter(token=tok)

			UsuarioInformacion.objects.filter(usuario=user).update(token=tok)
			useri = UsuarioInformacion.objects.get(usuario=user)
			foto = ''
			if useri.foto:
				foto = settings.DOMINIO + useri.foto.url

			user_json = [{
				"type": "user",
				"username": user.username,
				"user_id": user.id,
				"email": user.email,
				"foto": foto,
				"idioma": useri.idioma,
				"token": str(tok).replace('-','')
			}]
			return APIResponse(user_json, "Login", 1)
	else:
		return APIResponse("", "Token incorrect", 0)
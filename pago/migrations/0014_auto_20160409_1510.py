# -*- coding: utf-8 -*-
# Generated by Django 1.9.2 on 2016-04-09 21:10
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('pago', '0013_auto_20160409_1420'),
    ]

    operations = [
        migrations.AddField(
            model_name='compradetalle',
            name='tiempo_expiracion',
            field=models.IntegerField(default=6),
        ),
        migrations.AddField(
            model_name='compradetalle',
            name='tipo_expiracion',
            field=models.CharField(choices=[('mes', 'mes'), ('dia', 'dia')], default='mes', max_length=5),
        ),
    ]

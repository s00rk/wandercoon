# -*- coding: utf-8 -*-
# Generated by Django 1.9 on 2016-04-13 06:52
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('pago', '0018_compradetalle_expiracion_muestra'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='compradetalle',
            name='expiracion_muestra',
        ),
    ]

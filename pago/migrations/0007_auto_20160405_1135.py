# -*- coding: utf-8 -*-
# Generated by Django 1.9 on 2016-04-05 17:35
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('pago', '0006_producto_tipo'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='producto',
            name='tipo',
        ),
        migrations.AddField(
            model_name='producto',
            name='tipo_expiracion',
            field=models.CharField(choices=[('mes', 'mes'), ('dia', 'dia')], default='mes', max_length=5),
        ),
    ]

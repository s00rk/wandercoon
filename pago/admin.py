from django.contrib import admin

from .models import Compra, Producto, CompraDetalle, Combo, Carrito

@admin.register(Compra)
class CompraAdmin(admin.ModelAdmin):
	list_display = ('id', 'empresa', 'descripcion', 'cantidad', 'estado', 'fecha', 'vendedor')
	readonly_fields = ('fecha',)

	def empresa(self, obj):
		return '<a target="_blank" href="/admin/usuario/usuarioinformacion/' + str(obj.usuario.usuarioinformacion.id) + '/change/">' + obj.usuario.email + '</a>'
	empresa.allow_tags = True

@admin.register(Producto)
class ProductoAdmin(admin.ModelAdmin):
	list_display = ('nombre', 'descripcion', 'precio', 'tiempo_expiracion', 'tipo_expiracion')

@admin.register(Combo)
class ProductoAdmin(admin.ModelAdmin):
	list_display = ('nombre', 'descripcion', 'precio')
	filter_horizontal = ('productos',)

@admin.register(CompraDetalle)
class CompraDetalleAdmin(admin.ModelAdmin):
	list_display = ('compra', 'producto', 'precio', 'expiracion')

@admin.register(Carrito)
class CarritoAdmin(admin.ModelAdmin):
	list_display = ('usuario', 'combo', 'producto', 'local', 'cantidad', 'fecha')
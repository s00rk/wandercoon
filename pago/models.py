# -*- encoding: utf-8 -*-
from __future__ import unicode_literals

from django.conf import settings
from django.db import models
from django.template.loader import get_template
from django.template import Context
from django.dispatch.dispatcher import receiver
from django.db.models.signals import post_save
from django.core.mail import EmailMultiAlternatives

from django.contrib.auth.models import User
from usuario.models import UsuarioInformacion

from wandercoon_app.models import Lugar
from django.utils import timezone

from pdfs.views import generate_pdf
from decimal import Decimal
import threading

class Producto(models.Model):
	nombre = models.CharField(max_length=100)
	descripcion = models.TextField(blank=True)
	precio = models.DecimalField(max_digits=8, decimal_places=2)
	precio_promocion = models.DecimalField(max_digits=8, decimal_places=2, blank=True, null=True)
	tiempo_expiracion = models.IntegerField(default=6)
	tipo = (
		('mes', 'mes'),
		('dia', 'dia'),
	)
	tipo_expiracion = models.CharField(max_length=5, choices=tipo, default='mes')

	def __unicode__(self):
		return u'%s' % self.nombre

class Combo(models.Model):
	nombre = models.CharField(max_length=100)
	productos = models.ManyToManyField(Producto)
	descripcion = models.TextField()
	precio = models.DecimalField(max_digits=8, decimal_places=2)
	tiempo_expiracion = models.IntegerField(default=6)
	tipo = (
		('mes', 'mes'),
		('dia', 'dia'),
	)
	tipo_expiracion = models.CharField(max_length=5, choices=tipo, default='mes')

	def __unicode__(self):
		return u'%s' % self.nombre

class Compra(models.Model):
	descripcion = models.TextField(blank=True)
	usuario = models.ForeignKey(User)
	fecha = models.DateTimeField(auto_now_add=True, null=True)
	pagado = models.BooleanField(default=False)
	tipo_pago = models.CharField(max_length=100, blank=True, editable=False)
	codigo = models.CharField(max_length=100, blank=True, editable=False)
	cantidad = models.DecimalField(max_digits=8, decimal_places=2, null=True)
	ticket = models.CharField(max_length=200, blank=True)
	informacion_venta = models.TextField(blank=True)
	estado = models.CharField(max_length=20, blank=True)
	vendedor = models.ForeignKey(User, blank=True, null=True, related_name='vendedor')
	pdf_enviado = models.BooleanField(default=False)

	def detalles(self):
		return CompraDetalle.objects.filter(compra__pk=self.pk)

	def __unicode__(self):
		return u"%s %s" % (self.descripcion, self.usuario.email)

class CompraDetalle(models.Model):
	compra = models.ForeignKey(Compra)
	combo = models.ForeignKey(Combo, null=True)
	producto = models.ForeignKey(Producto, null=True)
	precio = models.DecimalField(max_digits=8, decimal_places=2)
	local = models.ForeignKey(Lugar, blank=True, null=True)
	expiracion = models.DateTimeField(default=timezone.now)
	tiempo_expiracion = models.IntegerField(default=6)
	tipo = (
		('mes', 'mes'),
		('dia', 'dia'),
	)
	tipo_expiracion = models.CharField(max_length=5, choices=tipo, default='mes')

	def __unicode__(self):
		return u"%s %s" % (self.compra.descripcion, self.compra.usuario.email)

class Carrito(models.Model):
	usuario = models.ForeignKey(User)
	combo = models.ForeignKey(Combo, null=True)
	producto = models.ForeignKey(Producto, null=True)
	local = models.ForeignKey(Lugar, null=True)
	cantidad = models.IntegerField(default=1)
	total = models.DecimalField(max_digits=8, decimal_places=2)
	fecha = models.DateTimeField(auto_now=True)

	def __unicode__(self):
		return "%s - %s" % (self.usuario, self.fecha)

@receiver(post_save , sender=Compra)
def enviar_correo(sender, instance, **kwargs):
	if instance.estado == 'venta_exitosa' and not instance.pdf_enviado and instance.pagado:
		try:
			Compra.objects.filter(pk=instance.pk).update(pdf_enviado=True)
			c = instance
			subtotal =c.cantidad/(Decimal(settings.IGV)+1)
			igv = c.cantidad - subtotal
			data = {'compra': c, 'nombre': (str(c.pk)+'.pdf'), 'igv': igv, 'subtotal': subtotal}
			pdf, html = generate_pdf('detalle_compra.html', data)
			admins = list(UsuarioInformacion.objects.filter(recibe_correo=True, usuario__is_staff=True).values_list('usuario__email', flat=True))
			from_email = settings.DEFAULT_FROM_EMAIL           
			subject = (u"Detalle de Compra")

			msg = EmailMultiAlternatives(subject, html, from_email, [c.usuario.email], bcc=admins)
			msg.content_subtype = "html"
			msg.attach("REPORTE-" + str(c.pk) + ".pdf", pdf, "application/pdf")
			msg.send()
		except:
			pass
		#Compra.objects.filter(pk=instance.pk).update(pdf_enviado=True)
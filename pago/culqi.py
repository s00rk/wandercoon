from Crypto.Cipher import AES
from Crypto import Random
import base64, string, re

def strspn(str1, str2, start=0, length=None):
    if not length: length = len(str1)
    return len(re.search('^[' + str2 + ']*', str1[start:start + length]).group(0))

class UrlAESCipher():
	key = ''
	#protected $cipher = MCRYPT_RIJNDAEL_128;
	mode = AES.MODE_CBC

	def __init__(self, key):
		self.setBase64Key(key)

	def setBase64Key(self, key):
		self.key = base64.b64decode(key)

	def sp4b5aaf( self ):
		return Random.new().read( 16 )

	def urlBase64Encrypt(self, sp755f74):
		sp810031 = self.pkcs5_pad(sp755f74, 16)
		sp1c42c6 = self.sp4b5aaf()
		cipher = AES.new( self.key, AES.MODE_CBC, sp1c42c6 )
		return self.base64_encode_url(sp1c42c6 + cipher.encrypt( sp810031 )).strip()

	def urlBase64Decrypt(self, sp755f74):
		spcc5dfb = self.base64_decode_url(sp755f74);
		sp1c42c6 = spcc5dfb[:16]
		sp20ef79 = spcc5dfb[16:]
		cipher = AES.new(self.key, AES.MODE_CBC, sp1c42c6 )
		return self.pkcs5_unpad(cipher.decrypt(sp20ef79)).strip()

	def pkcs5_pad(self, sp2dbdda, sp6bc36d):
		spf187c8 = sp6bc36d - len(sp2dbdda) % sp6bc36d
		return sp2dbdda + (chr(spf187c8) * spf187c8)

	def pkcs5_unpad(self, sp2dbdda):
		spf187c8 = ord(sp2dbdda[len(sp2dbdda) - 1]);
		if spf187c8 > len(sp2dbdda):
			return False
		if strspn(sp2dbdda, chr(spf187c8), len(sp2dbdda) - spf187c8) != spf187c8:
			return False
		return sp2dbdda[0: -1 * spf187c8]

	def base64_encode_url(self, sp36c362):
		return (base64.b64encode(sp36c362)).translate(string.maketrans('+/', '-_'))

	def base64_decode_url(self, sp36c362):
		return base64.b64decode(sp36c362.translate(string.maketrans('-_', '+/')))
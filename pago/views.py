# -*- coding: utf-8 -*-
from django.conf import settings
from django.shortcuts import render, redirect
from django.views.generic import View, TemplateView
from django.contrib import messages

from django.http import HttpResponse, JsonResponse
from django.views.generic import View
from django.shortcuts import render

from .models import Producto, Compra, CompraDetalle, Combo, Carrito
from wandercoon_app.models import Lugar
from django.utils import timezone

from .culqi import UrlAESCipher

import uuid, requests, json, sys, platform, datetime
from decimal import Decimal

def generarCompra(compra, request):
	us = request.user
	cantt = str(compra.cantidad/100).replace('.', '')
	informacion_venta = json.dumps({
		"codigo_comercio":"imCZUtaYTOnY",
		"numero_pedido": compra.pk,
		"moneda": "USD",
		"monto": cantt,
		"descripcion": compra.descripcion,
		"correo_electronico": us.email,
		"cod_pais": us.usuarioinformacion.pais,
		"ciudad": us.usuarioinformacion.ciudad,
		"direccion": us.usuarioinformacion.direccion,
		"num_tel": us.usuarioinformacion.telefono,
		"nombres": us.first_name,
		"apellidos": us.last_name,
		"id_usuario_comercio": us.pk,
		"sdk": {
			"v": "1.1.0",
			"lng_n": "python",
			"lng_v": sys.version,
			"os_n": platform.system(),
			"os_v": "3.10.0-327.4.5.el7.x86_64"
		}
	})

	cifrado = UrlAESCipher( settings.CULQI_KEY )
	informacion_venta = cifrado.urlBase64Encrypt( informacion_venta )
	data = {
		"codigo_comercio": "imCZUtaYTOnY",
		"informacion_venta": informacion_venta
	}
	headers = {'Content-Type': 'application/json', 'Accept': 'application/json'}
	r = requests.post(settings.CULQI_CREAR_VENTA, json=data, headers=headers)
	r.encoding = 'utf-8'
	data = json.loads(cifrado.urlBase64Decrypt( r.content ))
	informacion_venta = json.dumps({
		"codigo_comercio":"imCZUtaYTOnY",
		"ticket": data["ticket"]
	})
	informacion_venta = cifrado.urlBase64Encrypt( informacion_venta )
	if 'ticket' in request.session:
		del request.session['ticket']

	try:
		compra.estado = data["codigo_respuesta"]
		if compra.estado == 'venta_registrada':
			compra.ticket = data['ticket']
			compra.informacion_venta = informacion_venta
			request.session['ticket'] = compra.ticket
		else:
			compra.ticket = ''
			compra.informacion_venta = ''

		compra.save()
	except:
		pass
	return data

class PagoView(TemplateView):
	def post(self, request):
		respuesta = request.POST.get('respuesta')
		print respuesta
		if respuesta is None:
			return JsonResponse({})
		if respuesta == 'parametro_invalido':
			if 'ticket' in request.session:
				del request.session['ticket']
			return JsonResponse({ 'codigo_respuesta': respuesta, 'respuesta': respuesta }, safe=False)
		elif respuesta == 'venta_expirada':
			if 'ticket' in request.session:
				del request.session['ticket']
				r = 'true'
			return JsonResponse({ 'codigo_respuesta': respuesta, 'respuesta': r }, safe=False)
		elif respuesta == 'checkout_cerrado':
			if 'ticket' in request.session:
				del request.session['ticket']
				r = 'true'
			return JsonResponse({ 'codigo_respuesta': respuesta, 'respuesta': r }, safe=False)

		cifrado = UrlAESCipher( settings.CULQI_KEY )
		respuesta = cifrado.urlBase64Decrypt( str(respuesta) )
		respuesta = json.loads( respuesta )
		c = Compra.objects.filter(ticket=respuesta['ticket'])
		if c.exists():
			c = c.first()
			c.estado = respuesta['codigo_respuesta']
			c.save()
			cd = CompraDetalle.objects.filter(compra=c)

			if c.estado == 'venta_exitosa':
				c.pagado = True
				c.tipo_pago = 'Culqi'
				c.usuario.is_active = True
				c.save()
				x = Lugar.objects.filter(empresa=request.user.usuarioinformacion.empresa()).count()
				for comprad in cd:
					if comprad.combo is None:
						if comprad.local is None:
							x = x+1
							lugar = Lugar()
							lugar.empresa = request.user.usuarioinformacion.empresa()
							lugar.nombre = 'Nuevo Local ' + str(x)
							lugar.expiracion = comprad.expiracion
							lugar.save()
						elif comprad.local is not None:
							if "banner" in comprad.producto.nombre.lower():
								comprad.local.banner = True
								if comprad.local.expiro_banner():
									comprad.local.banner_expiracion = comprad.expiracion
								else:
									if comprad.tipo_expiracion == 'mes':
										comprad.local.banner_expiracion += (datetime.timedelta( int(comprad.tiempo_expiracion) *365/12))
									else:
										comprad.local.banner_expiracion += datetime.timedelta(days=comprad.tiempo_expiracion)
									comprad.expiracion = comprad.local.banner_expiracion

							elif "ruta" in comprad.producto.nombre.lower():
								comprad.local.ruta_recomendada = True
								if comprad.local.expiro_ruta():
									comprad.local.ruta_recomendada_expiracion = comprad.expiracion
								else:
									if comprad.tipo_expiracion == 'mes':
										comprad.local.ruta_recomendada_expiracion += (datetime.timedelta( int(comprad.tiempo_expiracion) *365/12))
									else:
										comprad.local.ruta_recomendada_expiracion += datetime.timedelta(days=comprad.tiempo_expiracion)
									comprad.expiracion = comprad.local.ruta_recomendada_expiracion

							elif "inscripci" in comprad.producto.nombre.lower():
								if comprad.local.expiro():
									comprad.local.expiracion = comprad.expiracion
								else:
									if comprad.tipo_expiracion == 'mes':
										comprad.local.expiracion += (datetime.timedelta( int(comprad.tiempo_expiracion) *365/12))
									else:
										comprad.local.expiracion += datetime.timedelta(days=comprad.tiempo_expiracion)

									comprad.expiracion = comprad.local.expiracion
							comprad.local.save()
							comprad.save()
					else:
						if comprad.local is None:
							if comprad.combo.pk <= 4:
								x = x + 1
								lugar = Lugar()
								lugar.empresa = request.user.usuarioinformacion.empresa()
								lugar.nombre = 'Nuevo Local ' + str(x)
								lugar.expiracion = comprad.expiracion
								lugar.save()
							if comprad.combo.pk == 4 or comprad.combo.pk == 3:
								lugar.banner_activado = True
								lugar.banner_expiracion = comprad.expiracion
								lugar.save()
						else:
							for p in comprad.combo.productos.order_by('id'):
								if "banner" in p.nombre.lower():
									comprad.local.banner = True
									comprad.local.verificacion = 'pendiente'
									if comprad.local.expiro_banner():
										comprad.local.banner_expiracion = comprad.expiracion
										comprad.local.verificacion_expiracion = comprad.expiracion
									else:
										if comprad.tipo_expiracion == 'mes':
											comprad.local.banner_expiracion += (datetime.timedelta( int(comprad.tiempo_expiracion) *365/12))
											if comprad.local.verificacion_expiracion is not None:
												comprad.local.verificacion_expiracion += (datetime.timedelta( int(comprad.tiempo_expiracion) *365/12))
										else:
											comprad.local.banner_expiracion += datetime.timedelta(days=comprad.tiempo_expiracion)
											comprad.local.verificacion_expiracion += datetime.timedelta(days=comprad.tiempo_expiracion)

										comprad.expiracion = comprad.local.banner_expiracion

								elif "ruta" in p.nombre.lower():
									comprad.local.ruta_recomendada = True
									if comprad.local.expiro_ruta():
										comprad.local.ruta_recomendada_expiracion = comprad.expiracion
									else:
										if comprad.tipo_expiracion == 'mes':
											comprad.local.ruta_recomendada_expiracion += (datetime.timedelta( int(comprad.tiempo_expiracion) *365/12))
										else:
											comprad.local.ruta_recomendada_expiracion += datetime.timedelta(days=comprad.tiempo_expiracion)
										comprad.expiracion = comprad.local.ruta_recomendada_expiracion

								elif "inscripci" in p.nombre.lower():
									if comprad.local.expiro():
										comprad.local.expiracion = comprad.expiracion
									else:
										if comprad.tipo_expiracion == 'mes':
											comprad.local.expiracion += (datetime.timedelta( int(comprad.tiempo_expiracion) *365/12))
										else:
											comprad.local.expiracion += datetime.timedelta(days=comprad.tiempo_expiracion)
										comprad.expiracion = comprad.local.expiracion
								comprad.local.save()
								comprad.save()


				messages.success(request, 'Compra Realizada exitosamente!')
				c.usuario.save()
				cart = Carrito.objects.filter(usuario=request.user)
				cart.delete()
				if 'ticket' in request.session:
					del request.session['ticket']
		else:
			return JsonResponse({ 'codigo_respuesta': 'venta_no_encontrada', 'respuesta': 'No se encontro ninguna venta registrada a tu nombre'}, safe=False)
		return JsonResponse({ 'codigo_respuesta': respuesta['codigo_respuesta'], 'respuesta': respuesta['mensaje_respuesta_usuario'] }, safe=False)

class PagarView(TemplateView):
	template_name = "pago/pagar.html"

	def get(self, request):
		cart = Carrito.objects.filter(usuario=request.user)
		c = Compra.objects.filter(ticket=request.session.get("ticket"))
		if c.exists():
			c = c.first()
			if c.estado == 'venta_exitosa':
				if 'ticket' in request.session:
					del request.session['ticket']
		else:
			if 'ticket' in request.session:
				del request.session['ticket']

		subtotal = Decimal('0')
		for p in cart:
			subtotal += p.total

		igv = subtotal * Decimal(settings.IGV)
		total = subtotal + igv
		return render(request, self.template_name, {'cart': cart, 'compra': c, 'igv': igv, 'total': total, 'subtotal':subtotal})

	def post(self, request):
		if request.POST.get("editar", None) is not None:
			request.user.first_name = request.POST.get("nombre")
			request.user.last_name = request.POST.get("apellido")
			request.user.usuarioinformacion.ciudad = request.POST.get("ciudad")
			request.user.usuarioinformacion.pais = request.POST.get("pais")
			request.user.usuarioinformacion.direccion = request.POST.get("direccion")
			request.user.usuarioinformacion.telefono = request.POST.get("telefono")
			request.user.usuarioinformacion.save()
			request.user.save()

			cart = Carrito.objects.filter(usuario=request.user).order_by('producto__pk', 'combo__pk')
			desc = ""
			total = Decimal(0)
			added = []
			for p in cart:
				total += p.total
				if p.combo is not None:
					if not p.combo.nombre in added:
						desc = desc + " - " + p.combo.nombre
						added.append( p.combo.nombre )
				else:
					if not p.producto.nombre in added:
						desc = desc + " - " + p.producto.nombre
						added.append( p.producto.nombre )
			us = request.user

			igv = total * Decimal(settings.IGV)
			total = total + igv
			compra = Compra()
			compra.descripcion = desc
			compra.usuario = us
			compra.cantidad = total
			compra.save()

			data = generarCompra(compra, request)

			if data['codigo_respuesta'] == 'venta_registrada':
				today = timezone.now()

				for it in cart:
					for x in range(0, it.cantidad):
						cd = CompraDetalle()

						precio = 0
						tipo_exp = 'mes'
						tiempo_exp = 0
						if it.combo is not None:
							precio = it.combo.precio
							tipo_exp = it.combo.tipo_expiracion
							tiempo_exp = it.combo.tiempo_expiracion
							cd.combo = it.combo
						else:
							tipo_exp = it.producto.tipo_expiracion
							tiempo_exp = it.producto.tiempo_expiracion
							if it.producto.precio_promocion is not None:
								precio = it.producto.precio_promocion
							else:
								precio = it.producto.precio
							cd.producto = it.producto

						cd.compra = compra
						cd.precio = precio
						if tipo_exp == 'mes':
							cd.expiracion = (today + datetime.timedelta( int(tiempo_exp) *365/12))
						else:
							cd.expiracion = (today + datetime.timedelta(days=tiempo_exp))


						if it.local is not None:
							cd.local = it.local
						else:
							cd.local = None
						cd.save()

				compra.save()
			else:
				messages.error(request, data['mensaje_respuesta_usuario'])

		return redirect("/panel/pagar/")

def add(request):
	cart = None
	combo = request.POST.get('combo', '')
	producto = request.POST.get('producto', '')
	lugar = request.POST.get('lugar', '')
	todos = request.POST.get('todos', '')
	todos_combo = request.POST.get('todos_combo', '')
	nombre = ''

	if len(combo) > 0:
		combo = Combo.objects.get(pk=combo)
		if len(lugar) > 0:
			lugar = Lugar.objects.get(pk=lugar)
			cart, created = Carrito.objects.get_or_create(usuario=request.user, combo=combo, local=lugar, defaults={'total': combo.precio})
		else:
			cart, created = Carrito.objects.get_or_create(usuario=request.user, combo=combo, local=None, defaults={'total': combo.precio})
		if not created:
			cart.cantidad = cart.cantidad + 1
		cart.total = cart.combo.precio * cart.cantidad
		cart.save()
		nombre = cart.combo.nombre
	elif len(todos_combo) > 0:
		combo = Combo.objects.get(pk=todos_combo)
		for local in Lugar.objects.filter(empresa__usuario=request.user).order_by('-id'):
			cart, created = Carrito.objects.get_or_create(usuario=request.user, combo=combo, local=local, defaults={'total': combo.precio})
			if not created:
				cart.cantidad = cart.cantidad + 1
			cart.total = cart.combo.precio * cart.cantidad
			cart.save()
		nombre = combo.nombre
	elif len(producto) > 0:
		producto = Producto.objects.get(pk=producto)
		if len(lugar) > 0:
			lugar = Lugar.objects.get(pk=lugar)
			cart, created = Carrito.objects.get_or_create(usuario=request.user, producto=producto, local=lugar, defaults={'total': producto.precio})
		else:
			cart, created = Carrito.objects.get_or_create(usuario=request.user, producto=producto, local=None, defaults={'total': producto.precio})
		if not created:
			cart.cantidad = cart.cantidad + 1
		if cart.producto.precio_promocion:
			cart.total = cart.producto.precio_promocion * cart.cantidad
		else:
			cart.total = cart.producto.precio * cart.cantidad
		cart.save()
		nombre = cart.producto.nombre
	elif len(todos) > 0:
		producto = Producto.objects.get(pk=todos)
		for local in Lugar.objects.filter(empresa__usuario=request.user).order_by('-id'):
			cart, created = Carrito.objects.get_or_create(usuario=request.user, producto=producto, local=local, defaults={'total': producto.precio})
			if not created:
				cart.cantidad = cart.cantidad + 1
			if cart.producto.precio_promocion:
				cart.total = cart.producto.precio_promocion * cart.cantidad
			else:
				cart.total = cart.producto.precio * cart.cantidad
			cart.save()
		nombre = producto.nombre

	if 'ticket' in request.session:
		del request.session['ticket']
	messages.success(request, 'Producto [ ' + nombre + ' ] Agregado')
	return HttpResponse("Added")


def remove(request):
	cart = None
	combo = request.POST.get('combo', '')
	producto = request.POST.get('producto', '')
	lugar = request.POST.get('lugar', '')
	nombre = ''

	if len(combo) > 0:
		if len(lugar) > 0:
			cart = Carrito.objects.filter(usuario=request.user, combo__pk=combo, local__pk=lugar)
		else:
			cart = Carrito.objects.filter(usuario=request.user, combo__pk=combo, local=None)
		if cart.exists():
			nombre = cart.first().combo.nombre
			cart.delete()
	elif len(producto) > 0:
		if len(lugar) > 0:
			cart = Carrito.objects.filter(usuario=request.user, producto__pk=producto, local__pk=lugar)
		else:
			cart = Carrito.objects.filter(usuario=request.user, producto__pk=producto, local=None)
		if cart.exists():
			nombre = cart.first().producto.nombre
			cart.delete()
	if 'ticket' in request.session:
		del request.session['ticket']
	messages.success(request, 'Producto [ ' + nombre + ' ] Eliminado')
	return HttpResponse("Removed")

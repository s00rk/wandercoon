from django.conf import settings
from django.db import models
from django.contrib.auth.models import User
from django.dispatch import receiver
from django.db.models.signals import post_save, post_delete
from datetime import date

from PIL import Image
import uuid, os
import datetime
from wandercoon_app.models import Empresa, Tag

class UsuarioInformacion(models.Model):
	usuario             = models.OneToOneField(User)
	about_me            = models.TextField(max_length=90, blank=True, default="")
	foto                = models.ImageField(upload_to='user', null=True, blank=True)
	cargo_representante = models.CharField(max_length=100, blank=True)
	direccion           = models.TextField(blank=True)
	facebook            = models.EmailField(null=True, blank=True)
	twitter             = models.CharField(null=True, blank=True, max_length=100)
	token               = models.UUIDField(null=True, blank=True, default=uuid.uuid4)
	token_active        = models.UUIDField(null=True, blank=True, default=uuid.uuid4)
	token_rest          = models.UUIDField(null=True, blank=True)
	TIPOS = (
		('Usuario', 'Viajero'),
		('Empresa', 'Empresa')
	)
	tipo                = models.CharField(max_length=100, default='Usuario', blank=True, choices=TIPOS)
	telefono            = models.CharField(max_length=20, blank=True)
	fecha_inscripcion   = models.DateField(auto_now=True)
	ruc                 = models.CharField(max_length=16, null=True, blank=True)
	ciudad              = models.CharField(max_length=50, null=True, blank=True, default='')
	pais                = models.CharField(max_length=10, blank=True, default='')
	contrasena          = models.CharField(max_length=50, blank=True, editable=False)
	recibe_correo       = models.BooleanField(default=False)
	IDIOMAS = (
		('es', 'Spanish'),
		('en', 'English')
	)
	intereses           = models.ManyToManyField(Tag, blank=True)

	#choice
	GENEROS = (
		('male', 'Hombre'),
		('female', 'Mujer')
	)

	#facebook things
	edad                = models.IntegerField(blank=True, null=True, default=0)
	genero              = models.CharField(max_length=8, default='male', choices=GENEROS, blank=True)
	estado_relacion     = models.CharField(max_length=15, default='single', blank=True)
	facebook_likes      = models.TextField(default='', blank=True)
	nacimiento          = models.DateField(default=date.today, null=True, blank=True)
	idioma              = models.CharField(max_length=100, default='es', choices=IDIOMAS, blank=True)
	facebook_token      = models.TextField(default='', blank=True)


	def empresa(self):
		empres = None
		e = Empresa.objects.filter(usuario=self.usuario)
		if e.exists():
			empres = e.first()
		return empres

	def get_foto(self):
		return settings.MEDIA_URL + self.foto

	def __unicode__(self):
		return u'%s' % self.usuario

@receiver(post_save, sender=User, dispatch_uid="create_useri")
def CrearUsuarioInformation(sender, instance, **kwargs):
	UsuarioInformacion.objects.get_or_create(usuario=instance)

@receiver(post_save , sender=UsuarioInformacion, dispatch_uid="update_foto")
def rename_user(sender, instance, update_fields, **kwargs):
	if instance.foto:
		ext = instance.foto.name.split('.')[-1]
		filename = 'user/{}.jpeg'.format(instance.pk)
		direccion = os.path.join(settings.MEDIA_ROOT, instance.foto.name)
		dir_file = os.path.join(settings.MEDIA_ROOT, filename)

		baseWidth = 300, 600
		img = Image.open(direccion)
		#widthPercent = (baseWidth / float(img.size[0]))
		#height = int((float(img.size[1]) * float(widthPercent)))
		#img = img.resize((baseWidth, height), Image.BILINEAR)
		img.thumbnail(baseWidth, Image.ANTIALIAS)
		img.save(dir_file, "jpeg")
		#img.save(dir_file)

		if str(dir_file) != (direccion):
			os.remove(direccion)
			UsuarioInformacion.objects.filter(pk=instance.pk).update(foto=filename)

@receiver(post_delete , sender=UsuarioInformacion, dispatch_uid="delete_foto")
def mymodel_delete(sender, instance, **kwargs):
	if instance.foto:
		instance.foto.delete(False)

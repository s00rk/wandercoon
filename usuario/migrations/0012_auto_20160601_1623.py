# -*- coding: utf-8 -*-
# Generated by Django 1.9.6 on 2016-06-01 22:23
from __future__ import unicode_literals

from django.db import migrations, models
import uuid


class Migration(migrations.Migration):

    dependencies = [
        ('usuario', '0011_auto_20160601_1621'),
    ]

    operations = [
        migrations.AlterField(
            model_name='usuarioinformacion',
            name='token',
            field=models.UUIDField(blank=True, default=uuid.uuid4, null=True),
        ),
    ]

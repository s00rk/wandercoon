# -*- coding: utf-8 -*-
# Generated by Django 1.9.6 on 2016-05-31 22:31
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('wandercoon_app', '0015_auto_20160417_1825'),
        ('usuario', '0008_auto_20160531_1626'),
    ]

    operations = [
        migrations.AddField(
            model_name='usuarioinformacion',
            name='intereses',
            field=models.ManyToManyField(blank=True, to='wandercoon_app.Tag'),
        ),
    ]

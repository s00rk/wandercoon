# -*- coding: utf-8 -*-
from django.conf import settings
from django.shortcuts import render, redirect
from django.contrib.sessions.models import Session
from django.contrib.auth import authenticate, login, logout
from django.http import JsonResponse
from django.contrib import messages
from django.views.generic import View, TemplateView
from django.core.mail import EmailMessage, EmailMultiAlternatives
from django.utils.encoding import smart_unicode

from django.template.loader import render_to_string
from django.core.mail import send_mail

from django.template.loader import get_template
from django.template import Context

from usuario.models import UsuarioInformacion
from wandercoon_app.models import Empresa, Lugar
from django.contrib.auth.models import User
from pago.models import Compra, CompraDetalle, Combo
from pago.culqi import UrlAESCipher

from decimal import Decimal
from django.utils import timezone
from datetime import datetime
import uuid, requests, json, sys, platform, datetime

def enviar_mail(em, usern, contr):
	to = em
	from_email = settings.DEFAULT_FROM_EMAIL
	subject = (u"Bienvenido a WanderCoon")
	html = get_template('email/activate.html')
	d = { 'username': usern,'contrasena': contr }
	html_content = html.render(d)

	msg = EmailMultiAlternatives(subject, '', from_email, [to])
	msg.attach_alternative(html_content, "text/html")
	msg.send()

def enviar_mail2(em, idcliente, cliente, telefono, direccion):
	to = em
	from_email = settings.DEFAULT_FROM_EMAIL
	subject = (u"Bienvenido a WanderCoon")
	html = get_template('email/email_sin_vendedor.html')
	d = {}
	html_content = html.render(d)

	msg = EmailMultiAlternatives(subject, '', from_email, [to])
	msg.attach_alternative(html_content, "text/html")
	msg.send()

	to = 'ventas.pmedina@gmail.com'
	from_email = settings.DEFAULT_FROM_EMAIL
	subject = (u"Inscripcion Nuevo Cliente " +  cliente)

	html_content = 'ID Cliente: ' + str(idcliente) + '<br />Cliente: ' + cliente + '<br />Telefono: ' + telefono + '<br />Correo: ' + em + '<br />Direccion: ' + direccion

	msg = EmailMultiAlternatives(subject, '', from_email, [to])
	msg.attach_alternative(html_content, "text/html")
	msg.send()

class PagoView(TemplateView):
	def get(self, request):
		return redirect('/')
	def post(self, request):
		respuesta = request.POST.get('respuesta')
		if respuesta is None:
			return JsonResponse({})
		cifrado = UrlAESCipher( settings.CULQI_KEY )
		respuesta = cifrado.urlBase64Decrypt( str(respuesta) )
		respuesta = json.loads( respuesta )
		c = Compra.objects.filter(ticket=respuesta['ticket'])
		if c.exists():
			c = c.first()
			c.estado = respuesta['codigo_respuesta']
			c.save()

			cd = CompraDetalle.objects.filter(compra=c).order_by('producto__pk')
			if c.estado == 'venta_exitosa':
				c.usuario.is_active = True
				c.pagado = True
				c.tipo_pago = 'Culqi'
				c.usuario.is_active = True
				c.save()
				lugar = Lugar()
				for comprad in cd:
					if "inscripci" in comprad.producto.nombre.lower():
						lugar.empresa = c.usuario.usuarioinformacion.empresa()
						lugar.nombre = 'Nuevo Local'
						lugar.expiracion = comprad.expiracion
						lugar.save()
					elif "ruta" in comprad.producto.nombre.lower():
						lugar.ruta_recomendada = True
						lugar.ruta_recomendada_expiracion = comprad.expiracion
						lugar.save()
					elif "banner" in comprad.producto.nombre.lower():
						lugar.banner_activado = True
						lugar.banner_expiracion = comprad.expiracion
						lugar.save()

				messages.success(request, 'Compra Realizada exitosamente!')
				del request.session['ticket']

			c.usuario.save()
			if c.estado == 'venta_exitosa':
				contrasena = c.usuario.usuarioinformacion.contrasena
				user = authenticate(username=c.usuario.username, password=contrasena)
				login(request, user)
				c.usuario.usuarioinformacion.contrasena = ''
				c.usuario.usuarioinformacion.save()

				enviar_mail(c.usuario.email, c.usuario.username, contrasena)

		return JsonResponse({ 'codigo_respuesta': respuesta['codigo_respuesta'], 'respuesta': respuesta['mensaje_respuesta'] }, safe=False)

class RegistroEmpresaView(TemplateView):
	template_name = 'web/registro.html'

	def post(self, request):
		if request.user.is_authenticated():
			return redirect('panel')

		username = request.POST.get('nombre')
		apellidos = request.POST.get('apellidos')
		correo = request.POST.get('correo')
		empresa = request.POST.get('empresa')
		cargo_empresa = request.POST.get('cargo_empresa')
		telefono = request.POST.get('telefono')
		direccion = request.POST.get('direccion')
		contrasena = request.POST.get('contrasena')
		contrasena2 = request.POST.get('contrasena2')
		ruc = request.POST.get('ruc')
		ciudad = request.POST.get('ciudad')
		pais = request.POST.get('pais')
		codigo_vendedor = request.POST.get('codigo_vendedor', None)


		err = False
		user = User.objects.filter(email=correo)
		if user.exists():
			messages.error(request, 'Ya existe un usuario con ese email')
			err = True
		useri = UsuarioInformacion.objects.filter(ruc=ruc)
		if useri.exists():
			messages.error(request, 'Ya existe un usuario con ese RUC')
			err = True
		if contrasena != contrasena2 or contrasena is None:
			messages.error(request, 'Las contraseñas no coinciden')
			err = True

		if err:
			return render(request, self.template_name)

		user = User.objects.create_user(correo, correo, contrasena)
		user.is_active = True
		user.first_name = username
		user.last_name = apellidos
		user.save()

		userInfo = UsuarioInformacion.objects.get(usuario=user)
		userInfo.telefono = telefono
		userInfo.cargo_representante = cargo_empresa
		userInfo.direccion = direccion
		userInfo.tipo = 'Empresa'
		userInfo.ruc = ruc
		userInfo.ciudad = ciudad
		userInfo.pais = pais
		userInfo.contrasena = contrasena
		userInfo.save()

		e = Empresa()
		e.nombre = empresa
		e.usuario = user
		e.save()

		local = Lugar()
		local.activo = False
		local.empresa = e
		local.nombre = 'Nuevo Local'
		local.save()

		enviar_mail(correo, correo, contrasena)
		#Enviar mail a Ale para que haga la validación.
		enviar_mail('urudff@gmail.com', correo, contrasena)

		#messages.success(request, "Gracias por inscribirse en Wandercoon. En las próximas 48 horas confirmaremos los datos ingresados y activaremos la membresía de su establecimiento.")
		user = authenticate(username=correo, password=contrasena)
		login(request, user)

		return redirect('/panel/mis-locales/' + local.slug + '/')


class LogoutView(View):
	def get(self, request):
		logout(request)
		return redirect('home')

class LoginView(TemplateView):
	template_name = 'login.html'
	def get(self, request):
		if request.user.is_authenticated():
			return redirect('panel')
		return render(request, self.template_name)

	def post(self, request):
		if request.user.is_authenticated():
			return redirect('panel')
		accion = request.POST.get('accion')
		if accion == 'entrar':
			usuario = request.POST.get('usuario')
			contrasena = request.POST.get('contrasena')

			if usuario is not None and contrasena is not None:
				user = authenticate(username=usuario, password=contrasena)
				if user is not None:
					if user.is_active:
						login(request, user)
						user.usuarioinformacion.contrasena = ''
						user.usuarioinformacion.save()
						return redirect('panel')
				messages.error(request, 'Usuario/Contraseña Incorrecta!')
				messages.set_level(request, messages.ERROR)
			else:
				messages.error(request, 'No se deben dejar campos vacios')
				messages.set_level(request, messages.ERROR)

		elif accion == 'restaurar':
			email_ = request.POST.get('email')
			if email_ is not None:
				user = User.objects.filter(email=email_)
				if not user.exists():
					messages.error(request, 'No existe ninguna cuenta con ese email!')
					messages.set_level(request, messages.ERROR)
				else:
					user = user.first()
					tok = uuid.uuid4()
					userInfo = UsuarioInformacion.objects.filter(token_rest=tok)
					while userInfo.exists():
						tok = uuid.uuid4()
						userInfo = UsuarioInformacion.objects.filter(token_rest=tok)

					UsuarioInformacion.objects.filter(usuario=user).update(token_rest=tok)
					url_rest = settings.DOMINIO + 'api/restaurar?token=' + str(tok).replace('-','')
					html_message = '<strong>Hola ' + user.username + '!</strong><br /><br />Se ha solicitado hacer una restauracion de tu clave, para hacerla puedes hacer click en el link a continuacion.<br /<br /><a href="' + url_rest + '">Cambiar Clave</a><br /><br />Si tu no hiciste esta peticion, solo ignora este mensaje.'
					send_m = EmailMessage('Restaurar Contraseña', html_message, settings.EMAIL_CONTACT, [email_])
					send_m.content_subtype = "html"
					send_m.send()
					messages.success(request, 'Se te ha enviado un correo para restaurar tu cuenta!')
					messages.set_level(request, messages.SUCCESS)
			else:
				messages.error(request, 'No se deben dejar campos vacios')
				messages.set_level(request, messages.ERROR)
		elif accion == 'registro':
			nombre = request.POST.get('nombre')
			apellido = request.POST.get('apellido')
			username = request.POST.get('usuario')
			contrasena = request.POST.get('contrasena')
			re_contrasena = request.POST.get('re_contrasena')
			email = request.POST.get('email')
			telefono = request.POST.get('telefono')

			if username is None or contrasena is None or re_contrasena is None or email is None or telefono is None or nombre is None or apellido is None:
				messages.error(request, 'No se deben dejar campos vacios')
				messages.set_level(request, messages.ERROR)
			elif contrasena != re_contrasena:
				messages.error(request, 'Las contraseñas no coinciden')
				messages.set_level(request, messages.ERROR)
			else:

				user = User.objects.filter(username=username)
				if user.exists():
					messages.error(request, 'Ya existe un usuario con ese nombre')
					messages.set_level(request, messages.ERROR)
				else:
					user = User.objects.filter(email=email)
					if user.exists():
						messages.error(request, 'Ya existe un usuario con ese email')
						messages.set_level(request, messages.ERROR)
					else:

						user = User.objects.create_user(username, email, contrasena)
						userInfo = UsuarioInformacion.objects.get(usuario=user)
						userInfo.telefono = telefono
						userInfo.save()
						login(request, user)
						user.usuarioinformacion.contrasena = ''
						user.usuarioinformacion.save()
						return redirect('panel')

		return render(request, self.template_name)

class MiPerfilView(TemplateView):
	template_name = 'usuario/perfil.html'

	def get(self, request):
		empresa = Empresa.objects.filter(usuario=request.user).first()
		userInfo = request.user.usuarioinformacion
		data = {
			'foto': userInfo.foto,
			'empresa': empresa.nombre,
			'ruc': userInfo.ruc,
			'direccion': userInfo.direccion,
			'nombre_representante': request.user.first_name,
			'apellido_representante': request.user.last_name,
			'cargo_representante': userInfo.cargo_representante,
			'correo': request.user.email,
			'telefono': userInfo.telefono
		}
		return render(request, self.template_name, data)

	def post(self, request):
		contrasena = request.POST.get('last_password')
		new_contrasena = request.POST.get('contrasena')
		"""
		user = authenticate(username=request.user.username, password=contrasena)
		if user is None:
			messages.error(request, 'La contraseña actual no es correcta!')
			return redirect('mi-perfil')
		"""

		empresa = Empresa.objects.filter(usuario=request.user).first()
		userInfo = request.user.usuarioinformacion
		userInfo.direccion = request.POST.get('direccion')
		userInfo.cargo_representante = request.POST.get('cargo_representante')
		userInfo.telefono = request.POST.get('telefono')
		userInfo.foto = request.FILES.get('foto')
		userInfo.save()
		request.user.first_name = request.POST.get('nombre_representante')
		request.user.last_name = request.POST.get('apellido_representante')
		request.user.email = request.POST.get('correo')
		request.user.save()
		messages.success(request, 'Datos Actualizados')

		if new_contrasena is not None and new_contrasena != '':
			user.set_password(new_contrasena)
			user.save()
			logout(request)
			[s.delete() for s in Session.objects.all() if s.get_decoded().get('_auth_user_id') == user.id]
			return redirect('/panel/')


		return redirect('mi-perfil')

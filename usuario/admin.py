from django.contrib import admin

from .models import UsuarioInformacion

@admin.register(UsuarioInformacion)
class UsuarioInformacionAdmin(admin.ModelAdmin):
	list_display = ('usuario', 'facebook', 'twitter', 'token')
	filter_horizontal = ('intereses',)

from django.conf.urls import include, url, patterns
from django.contrib.auth.decorators import login_required
from django.contrib import admin
from django.conf import settings
from django.conf.urls.static import static
from django.views.generic import TemplateView
from django.views.decorators.csrf import csrf_exempt

from wandercoon_app import api_urls

from usuario.views import LoginView, LogoutView, RegistroEmpresaView, PagoView, MiPerfilView
from wandercoon_app.views import HomeView, PanelView, LocalView, LocalEditView, LocalDeleteView, LocalNewView, ComprasView
from pago import views as views_pago

urlpatterns = [
	url(r'^$', HomeView.as_view(), name='home'),
	url(r'^registro/empresa/', RegistroEmpresaView.as_view(), name='registro-empresa'),
	url(r'^legal/$', TemplateView.as_view(template_name='web/legal.html')),
	url(r'^en/$', TemplateView.as_view(template_name='web/eng/index.html')),

	url(r'^pagar/$', PagoView.as_view(), name='pagar'),
	url(r'^carrito/agregar/$', views_pago.add, name='carrito-agregar'),
	url(r'^carrito/eliminar/$', views_pago.remove, name='carrito-eliminar'),

	url(r'^api/', include(api_urls.urls)),

	url(r'^login/', LoginView.as_view(), name='login'),
	url(r'^logout/', LogoutView.as_view(), name='logout'),

	url(r'^panel/$', login_required(PanelView.as_view()), name='panel'),
	url(r'^panel/mis-locales/$', login_required(LocalView.as_view()), name='mis-locales'),
	url(r'^panel/mis-locales/nuevo/$', login_required(LocalNewView.as_view()), name='mis-locales-new'),
	url(r'^panel/mis-locales/eliminar/(?P<slug>[-\w]+)/$', login_required(LocalDeleteView.as_view()), name='mis-locales-delete'),
	url(r'^panel/mis-locales/([-\w]+)/$', login_required(LocalEditView.as_view()), name='mis-locales-edit'),
	url(r'^panel/mis-compras/', login_required(ComprasView.as_view()), name='mis-compras'),
	url(r'^panel/perfil/$', login_required(MiPerfilView.as_view()), name='mi-perfil'),
	url(r'^panel/pagar/$', login_required(views_pago.PagarView.as_view()), name='panel-pagar'),
	url(r'^panel/pagar/pagado/$', login_required(views_pago.PagoView.as_view()), name="pagar-pagado"),

    url(r'^admin/', include(admin.site.urls)),
]# + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

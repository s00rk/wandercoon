class LanguageMiddleware(object):
	# Check what language will be response
	def process_request(self, request):
		idioma = request.GET.get('lang')

		if idioma == "EN" or idioma == "en":
			request.idioma = "EN"
		else:
			request.idioma = "ES"		
		
		return None
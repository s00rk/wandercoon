import datetime
import os

# -*- encoding: utf-8 -*-
from django.conf import settings
from django.http import HttpResponse
from django.template import Context
from django.template.loader import get_template

import pdfkit


def generate_pdf(template, data={}):
	pdf = data['nombre']
	template = get_template('pdfs/' + template)
	htmlS = template.render(data)
	html = u"%s" % htmlS
	pdfkit.from_string(html, pdf)
	f = open(pdf, 'rb')
	result = f.read()
	f.close()
	os.remove(pdf)
	return result, htmlS